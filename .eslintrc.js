const { THEME } = require('./globals.config.json')

const themeGlobals = function () {
  const tmp = {}
  for (let prop in THEME) {
    tmp[prop] = false;
  }
  return tmp
}

module.exports = {
  root: true,
  globals: {
    ...themeGlobals()
  },
  env: {
    node: true
  },
  plugins: [
    '@intlify/vue-i18n'
  ],
  extends: [
    'plugin:vue/essential',
    '@vue/standard'
  ],
  rules: {
    'no-console': process.env.NODE_ENV === 'production' ? 'error' : 'off',
    'no-debugger': process.env.NODE_ENV === 'production' ? 'error' : 'off',
    '@intlify/vue-i18n/no-raw-text': ['error', {
      // "ignoreNodes": ["md-icon", "v-icon"],
      "ignorePattern": '^[-#:()&"\'.!?]+$',
      "ignoreText": ['English', 'Français']
    }],
    '@intlify/vue-i18n/no-html-messages': 'error',
    '@intlify/vue-i18n/no-missing-keys': 'error',
    '@intlify/vue-i18n/no-v-html': 'warn'
  },
  parserOptions: {
    parser: 'babel-eslint'
  },
  overrides: [
    {
      files: [
        '**/__tests__/*.{j,t}s?(x)',
        '**/tests/unit/**/*.spec.{j,t}s?(x)'
      ],
      env: {
        jest: true
      }
    },
    // e2e
    {
      files: ['**/tests/e2e/**/*.{j,t}s?(x)'],
      rules: {
        'no-console': ['off']
      }
    },
    // To disable vue-i18n rules for icons components
    {
      files: ['src/components/elements/icons/*.vue'],
      rules: {
        '@intlify/vue-i18n/no-raw-text': ['off']
      }
    }
  ],
  settings: {
    '@intlify/vue-i18n': {
      localeDir: './src/i18n/*.json' // extention is glob formatting!
    },
    'vue-i18n': {
      localeDir: './src/i18n/*.json' // extention is glob formatting!
    }
  }
}

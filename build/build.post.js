/* eslint-disable */

const CONFIG = require('../globals.config.json')

const MYSTATION_VERSION = CONFIG.VERSION.CURRENT

// --

;(async () => {
  // Start
  console.log('Post-build script: started...\n')

  console.log('Build finish for version: ' + MYSTATION_VERSION + '\n')

  // End
  console.log('Post-build script: exit')
})()

/* eslint-disable */

const CONFIG = require('../globals.config.json')

const MYSTATION_VERSION = CONFIG.VERSION.CURRENT

// --

;(async () => {
  // Start
  console.log('Pre-build script: started...\n')

  console.log('MyStation UI current version: ' + MYSTATION_VERSION + '\n')

  // End
  console.log('Pre-build script: exit')
})()

![MyStation logo](http://mystation.fr/static/img/logo.png)

# MyStation - User Interface

[![mystation](https://img.shields.io/static/v1?label=&message=MyStation&color=191919&logo=data:image/png;base64,AAABAAEAEBAAAAEAIABoBAAAFgAAACgAAAAQAAAAIAAAAAEAIAAAAAAAAAQAABILAAASCwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADTOpAA0zqQMGLK1AiblVxIe4VsAFKq85DDKqAg0zqQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAANM6kADTOpAA0zqSUNM6mNCzGqx4e3VtqAsVvXCzCrxw0zqYMNM6keDTOpAA0zqQAAAAAAAAAAAA40qAAMM6kADTOpFg0zqXQNM6nKDTOpnAQpryy+9DGRvPIyggAlsjENM6miDTOpxQ0zqWcNM6kQDTOpAA81pgCSvQAFQmpiUxE4o8ANM6m0DTOpRg0zqQan2kEAv/UwkL/1MICs4D0ADTOpBw0zqUwNM6mzEjijtElxWUa24gADfqcUIHOdIs40XHVxAB7EDQ0zqQCo3D8Av/UwAL/1MJC/9TCAv/UwAA40qQANM6kDCi+uVyNJjOtxmiXYgqwOIX+pEiN/qRLEgKoRKICqEQAAAAAAv/UwAL/1MAC/9TCQv/UwgP//AAANM6ktDTOpmwwxq8wtVH+ZfacVzYCqESd/qRIjf6kSxH+pEih/qRIAAAAAAL/1MAC/9TAAwPYvj6XYQpcGK655DTOpzw0zqZkHLLIpfacVJ3+pEsV/qRInf6kSI3+pEsR/qRIof6kSALHlNQDB+DAAvPIyEsL4LquKu1T5HUWevQMosEAAG7gEcJklAH+pEid/qRLGf6kSJ3+pEiN/qRLEf6kSKIm1FwC78TEIvvQxT770ML+88S/Pue4y0rnuNMPG/CtJx/4oB4q1FQB/qRInf6kSxn+pEid/qRIjfqgSw4GrEyrB9zIzvvQwp7zxMNi26zCEqtwvHbbqKhq88i5+vvQv07/0MKPB9zExgasTKH6oEsV/qRInf6kSIou3F8y26imnvvQw17jtMaGu4jEwfqs3ApfHNACu4SgAn9AhAbnuLSy98i+ZwPYv0rbqKKOKthfOf6kSJ53MJR+r3jHamMtH6JnLRFTf/wgEvu4jAAAAAAAAAAAAAAAAAAAAAADR+BwA//8AA5vNRVGZy0jnq94w3JnIICLE+BMFZJFnWRpBn8UJL6yrDDKqOw40qAMNM6kAAAAAAAAAAAANM6kADjSoBAwyqkMJL6uxG0KewWqYYlPR/wAEARDOACJldgAMMascDTOpgw0zqc8NM6mNDTOpIg0zqQARN6YADTOpJw0zqZYNM6nPDTOpfAwwqxgYTpAAAA3NAAAAAAAAAAAADTOpAA0zqQINM6kyDTOpog0zqcdgjW52W4hxfA0zqcoNM6mcDTOpLQ0zqQENM6kAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADTOpAAowqwkPNadbjsBP24/ATdcPNaZUCS+tBw0zqQAAAAAAAAAAAAAAAAAAAAAA+B8AAOAPAADAAwAAAkAAAA5gAAAeQAAAHgAAABwIAAAQCAAAAAAAAAGAAAAH4AAAA8AAAMEDAADgBwAA+B8AAA==)](http://mystation.fr)
[![mystation-ui](https://img.shields.io/static/v1?labelColor=3D373A&label=mystation-ui&message=v1.0.0&color=191919)](https://gitlab.com/mystation/mystation-ui/)
[![Discord](https://img.shields.io/discord/729028505123291228?label=Discord&logo=discord&logoColor=white)](https://discordapp.com/channels/729028505123291228)
[![pipeline status](https://gitlab.com/mystation/mystation-ui/badges/master/pipeline.svg)](https://gitlab.com/mystation/mystation-ui/commits/master)

> Please consult [documentation](http://developers.mystation.fr/)

## Get started

```bash
# Clone this repository
git clone https://gitlab.com/mystation/mystation-ui.git
# Install packages
npm install
# Take a coffee ;)
```

## Start hot development server

```bash
npm run dev
```

See also [configuration](#Configuration)

## Build

```bash
# Build: build + generate docs
npm run build
# Only vue-cli build
npm run build:only
```

## Release

> Local repository must be clean

- 1. Update CHANGELOG
- 2. New release (run command below)

```bash
# New release
npm run release
```

## Documentation

```bash
# Generate all API docs
npm run docs:generate
# Generate only readme
npm run readme
```

## Command list

> See also package.json file

```bash
npm run build:only # vue cli build
npm run build # Build
npm run prebuild # Do nothing
npm run postbuild # Do nothing
npm run dev # npm run serve
npm run predocs:generate # npm run readme
npm run docs:generate # Generate docs + readme
npm run info:version # Print current mystation-ui version
npm run version:new # Increments version (in globals.config.json & package.json) + git commit
npm run version:tag # Create new git tag
npm run release # Create new release
npm run serve # vue cli serve (development)
npm run readme # Generate readme based on template
npm run test:unit # Start unit tests
npm run test:e2e # Start e2e tests
npm run lint # Lint code
```

## Configuration

```
mystation-ui
├─ ...
├─ src
│  ├─ config
│  │  ├─ config.js
└─ ...
```

### Options

```javascript
module.exports = {
  /**
   * Internationalization
   */
  I18N: {
    /**
     * Must be 'fr' or 'en'
     * @default 'fr'
     * @type {string}
     */
    LOCALE: 'fr',
    /**
     * Must be 'fr' or 'en'
     * @default 'en'
     * @type {string}
     */
    FALLBACK_LOCALE: 'en'
  },
  /**
   * APIs
   */
  API: {
    /**
     * Server API as Axios Instance options
     * @type {Object}
     */
    myStationAPI: {
      'baseURL': '/api',
      'allowedMethods': ['GET', 'PUT', 'PATCH', 'POST', 'DELETE', 'OPTIONS'],
      'headers': {
        'X-Requested-With': 'XMLHttpRequest'
      }
    },
    /**
     * MyStation Web API as Axios Instance options
     * @type {Object}
     */
    myStationWebAPI: {
      'baseURL': 'http://localhost:8001/api',
      'allowedMethods': ['GET', 'PUT', 'PATCH', 'POST', 'DELETE', 'OPTIONS'],
      'headers': {
        'X-Requested-With': 'XMLHttpRequest'
      }
    }
  },
  /**
   * Authentication
   */
  AUTH: {
    /**
     * oAuth access token identifier (in browser LocalStorage)
     * @default 'ms_access_token'
     * @type {string}
     */
    ACCESS_TOKEN_NAME: 'ms_access_token',
    /**
     * oAuth refresh token identifier (in browser LocalStorage)
     * @default 'ms_refresh_token'
     * @type {string}
     */
    REFRESH_TOKEN_NAME: 'ms_refresh_token'
  },
  /**
   * Development options
   */
  DEVELOPMENT: {
    /**
     * Define server API in development mode
     */
    API: {
      myStationAPI: {
        URL: 'http://localhost:8000', // Local server url
        APIBaseURL: 'http://localhost:8000/api'
      }
    }
    /**
     * Enable app development mode
     * Use it when app development
     */
    // appDevelopment: {
    //   myapp: {
    //     URL: 'http://localhost:8081'
    //   }
    // }
  }
}

```

## Dependencies

<details>
<summary>Global</summary>

<%= dependencies %>

</details>

<details>
<summary>Dev</summary>

<%= devDependencies %>

</details>
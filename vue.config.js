const path = require('path')
const webpack = require('webpack')

const { THEME, VERSION } = require('./globals.config.json')

// JS output subdirectory
const jsOutputPath = 'js'
// CSS output subdirectory
const cssOutputPath = 'css'

// Build Global 'THEME' object for define plugin (webpack 'constants') (see below).
const themeGlobalsForDefinePlugin = function () {
  const tmp = {}
  for (let prop in THEME) {
    tmp[prop] = `'${THEME[prop]}'`
  }
  return tmp
}

// Build Global 'THEME' sass vars definitions
// to inject in all sass files (see below)
const themeGlobalsForSassInjection = function () {
  let str = ''
  for (let prop in THEME) {
    str += `$${prop}: ${THEME[prop]};\n`
  }
  return str
}

// Define rules for expose loader
// to make accessible some dependencies
// in window.lib.<sharedDep>
const exposeLoaderRules = [
  // Axios dependencies
  { test: require.resolve('axios'), use: 'expose-loader?lib.axios' },
  // Color dependencies
  { test: require.resolve('color'), use: 'expose-loader?lib.color' },
  // Lodash dependencies
  { test: require.resolve('lodash.camelcase'), use: 'expose-loader?lib._.camelcase' },
  { test: require.resolve('lodash.capitalize'), use: 'expose-loader?lib._.capitalize' },
  { test: require.resolve('lodash.kebabcase'), use: 'expose-loader?lib._.kebabcase' },
  { test: require.resolve('lodash.maxby'), use: 'expose-loader?lib._.maxBy' },
  { test: require.resolve('lodash.minby'), use: 'expose-loader?lib._.minBy' },
  { test: require.resolve('lodash.snakecase'), use: 'expose-loader?lib._.snakecase' },
  { test: require.resolve('lodash.sortby'), use: 'expose-loader?lib._.sortBy' },
  { test: require.resolve('lodash.uppercase'), use: 'expose-loader?lib._.uppercase' }
  // Vue dependencies
  // { test: require.resolve('vue'), use: 'expose-loader?Vue' } // Doesn't work here. See src/main.js
]

module.exports = {
  devServer: {
    overlay: {
      warnings: true,
      errors: true
    }
  },
  configureWebpack: {
    // Output
    output: {
      filename: `${jsOutputPath}/[name].${VERSION.CURRENT}.js`,
      chunkFilename: `${jsOutputPath}/[name].${VERSION.CURRENT}.js`
    },
    resolve: {
      alias: {
        'vue$': 'vue/dist/vue.esm.js',
        '@': path.resolve(__dirname, 'src'),
        '~': path.resolve(__dirname, ''), // Root directory
        '#styles': path.resolve(__dirname, 'src/scss'),
        '#mystation': path.resolve(__dirname, 'src/mystation')
      }
    },
    module: {
      rules: [
        ...exposeLoaderRules
      ]
    },
    // Code splitting
    optimization: {
      splitChunks: {
        // cacheGroups: {
        //   commons: {
        //     test: /[\\/]node_modules[\\/]/,
        //     // cacheGroupKey here is `commons` as the key of the cacheGroup
        //     name (module, chunks, cacheGroupKey) {
        //       const moduleFileName = module.identifier().split('/').reduceRight(item => item)
        //       const allChunksNames = chunks.map((item) => item.name).join('~')
        //       return `${cacheGroupKey}-${allChunksNames}-${moduleFileName}`
        //     },
        //     chunks: 'all'
        //   }
        // }
      }
    },
    plugins: [
      // To define some global variables
      new webpack.DefinePlugin({
        ...themeGlobalsForDefinePlugin(),
        '__UI_VERSION__': `'${VERSION.CURRENT}'`
      })
    ]
  },
  css: {
    extract: {
      // Options similar to the same options in webpackOptions.output
      // both options are optional
      filename: `${cssOutputPath}/[name].${VERSION.CURRENT}.css`,
      chunkFilename: `${cssOutputPath}/[id].${VERSION.CURRENT}.css`
    },
    loaderOptions: {
      scss: {
        // To have access to variables in all CHILD SASS files
        prependData: `
          ${themeGlobalsForSassInjection()}
          @import "~#styles/_variables.scss";
        `
      }
    }
  }
}

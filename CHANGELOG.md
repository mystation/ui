# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [1.0.0] - 2019-07-07
First stable version

[Unreleased]: https://gitlab.com/mystation/mystation-ui/compare/v1.0.0...master
[1.0.1]: https://gitlab.com/mystation/mystation-ui/compare/v1.0.0...v1.0.1
[1.0.0]: https://gitlab.com/mystation/mystation-ui/tags/v1.0.0
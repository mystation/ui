/*
  MyStation root app script
*/

/* Packages */

// Vue
import Vue from 'vue'
// MyStation Global API
import MyStation from '#mystation'
// iso-i18n
import languages from '@cospired/i18n-iso-languages'
// Font awesome icons
import 'vue-awesome/icons'

/* Config */
import CONFIG from '~/globals.config.json'

/* Plugins */

// Bootstrap-vue
import BootstrapVue from 'bootstrap-vue'
// Perfect scrollbar
import PerfectScrollbar from 'vue2-perfect-scrollbar'
import 'vue2-perfect-scrollbar/dist/vue2-perfect-scrollbar.css' // CSS for Perfect Scrollbar

/* Components */

// Main component
import App from '@/components/App'
// MyStation specific components
import Logo from '@/components/elements/icons/Logo'
import MsModal from '@/components/elements/modals/MsModal'
import MsButton from '@/components/elements/buttons/MsButton'
import MsCollapse from '@/components/elements/collapses/MsCollapse'
import MsColorPicker from '@/components/elements/color-pickers/MsColorPicker'
import AppContainer from '@/components/mainframe/AppContainer'
import Pancake from '@/components/mainframe/dashboard/Pancake'
import IconBasicLoad from '@/components/elements/icons/IconBasicLoad'
import Loading1 from '@/components/elements/loadings/Loading1'
// Others
import Icon from 'vue-awesome/components/Icon'
import VueJsonPretty from 'vue-json-pretty'
import vSelect from 'vue-select'

/* Mixins */

// Main mixin
import MainMixin from '@/mixins/MainMixin'

/* Directives */

// Button ripple directive
import Ripple from 'vue-ripple-directive'

/* END IMPORTS */

// Vue config
// Hide production message in the web browser console
Vue.config.productionTip = process.env.NODE_ENV === 'development'
// Define error handler
/* TO DO: error handler doesn't catch in app store module... */
Vue.config.errorHandler = MyStation.system.errorHandler

// Support french & english languages.
languages.registerLocale(require('@cospired/i18n-iso-languages/langs/en.json'))
languages.registerLocale(require('@cospired/i18n-iso-languages/langs/fr.json'))

// Plugins utilisation
Vue.use(BootstrapVue)
Vue.use(PerfectScrollbar)

// Mixins
Vue.mixin(MainMixin)

// Directives activation
Ripple.color = 'rgba(255, 255, 255, 0.35)' // Define global color for ripple
Vue.directive('ripple', Ripple)

// Components registration
Vue.component('Logo', Logo)
Vue.component('MsModal', MsModal)
Vue.component('MsButton', MsButton)
Vue.component('MsCollapse', MsCollapse)
Vue.component('MsColorPicker', MsColorPicker)
Vue.component('AppContainer', AppContainer)
Vue.component('Pancake', Pancake)

Vue.component('v-icon', Icon)
Vue.component('IconBasicLoad', IconBasicLoad)
Vue.component('Loading1', Loading1)
Vue.component('VueJsonPretty', VueJsonPretty)
Vue.component('v-select', vSelect)

// Put Vue into window.Vue
// expose-loader doesn't work with 'vue' import...
window.lib.Vue = Vue

// Overrire default window error manager
window.onerror = function (msg, url, noLigne, noColonne, erreur) {
  var chaine = msg.toLowerCase()
  var souschaine = 'script error'
  if (chaine.indexOf(souschaine) > -1) {
    alert('Script Error : voir la Console du Navigateur pour les Détails')
  } else {
    var message = [
      'Message : ' + msg,
      'URL : ' + url,
      'Ligne : ' + noLigne,
      'Colonne : ' + noColonne,
      'Objet Error : ' + JSON.stringify(erreur)
    ].join(' - ')
    alert(message)
  }
  return false
}

// New MyStation Instance
window.__MYSTATION_VUE_INSTANCE__ = new Vue({
  // Name
  name: '__MYSTATION_VUE_INSTANCE__',
  // Main informations
  el: '#app',
  template: '<App/>',
  components: {
    App // Main App component
  },
  // Data for root instance
  data: function () {
    // Return mapped globals
    return {
      ROOT_GLOBALS: {
        __UI_VERSION__: CONFIG.VERSION.CURRENT,
        ...CONFIG.THEME
      },
      langs: languages.langs().map((lang) => { return { lang: lang, name: languages.getName(lang, lang) } }),
      class: {
        ...MyStation.class
      },
      components: {
        AppContainer: MyStation.components.AppContainer,
        BaseAppComponent: MyStation.components.BaseAppComponent
      }
    }
  },
  // Methods for root instance
  methods: {
    // Boot
    boot: MyStation.system.boot
  },
  // Internationalization
  i18n: MyStation.system.I18n,
  // Store
  store: MyStation.system.Store,
  // Services (Vue reactive plugins)
  APIManager: MyStation.services.APIManager,
  AppManager: MyStation.services.AppManager,
  ErrorManager: MyStation.services.ErrorManager,
  LoadingManager: MyStation.services.LoadingManager,
  Logger: MyStation.services.Logger,
  // 'created' Vue hook
  async created () {
    // Boot
    this.boot()
  }
})

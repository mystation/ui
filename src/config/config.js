module.exports = {
  /**
   * Internationalization
   */
  I18N: {
    /**
     * Must be 'fr' or 'en'
     * @default 'fr'
     * @type {string}
     */
    LOCALE: 'fr',
    /**
     * Must be 'fr' or 'en'
     * @default 'en'
     * @type {string}
     */
    FALLBACK_LOCALE: 'en'
  },
  /**
   * APIs
   */
  API: {
    /**
     * Server API as Axios Instance options
     * @type {Object}
     */
    myStationAPI: {
      'baseURL': '/api',
      'allowedMethods': ['GET', 'PUT', 'PATCH', 'POST', 'DELETE', 'OPTIONS'],
      'headers': {
        'X-Requested-With': 'XMLHttpRequest'
      }
    },
    /**
     * MyStation Web API as Axios Instance options
     * @type {Object}
     */
    myStationWebAPI: {
      // 'baseURL': 'http://localhost:8001/api',
      'baseURL': 'http://mystation.fr/api',
      'allowedMethods': ['GET', 'PUT', 'PATCH', 'POST', 'DELETE', 'OPTIONS'],
      'headers': {
        'X-Requested-With': 'XMLHttpRequest'
      }
    },
    /**
     * Google DNS (8.8.8.8) as Axios Instance options.
     * (For network checking)
     * @type {Object}
     */
    googleDNS: {
      'baseURL': 'https://8.8.8.8/',
      'allowedMethods': ['GET'],
      'headers': {
        'X-Requested-With': 'XMLHttpRequest'
      }
    }
  },
  /**
   * Authentication
   */
  AUTH: {
    /**
     * oAuth access token identifier (in browser LocalStorage)
     * @default 'ms_access_token'
     * @type {string}
     */
    ACCESS_TOKEN_NAME: 'ms_access_token',
    /**
     * oAuth refresh token identifier (in browser LocalStorage)
     * @default 'ms_refresh_token'
     * @type {string}
     */
    REFRESH_TOKEN_NAME: 'ms_refresh_token'
  },
  /**
   * Development options
   */
  DEVELOPMENT: {
    /**
     * Define server API in development mode
     */
    API: {
      myStationAPI: {
        URL: 'http://localhost:8000', // Local server url
        APIBaseURL: 'http://localhost:8000/api'
      }
    }
    // /**
    //  * Enable app development mode
    //  * Use it when app development
    //  */
    // appDevelopment: {
    //   myapp: {
    //     URL: 'http://localhost:8081'
    //   }
    // }
  }
}

/**
 * @vuepress
 * ---
 * title: updateApp
 * headline: "MyStation module: updateApp"
 * sidebarDepth: 0 # To disable auto sidebar links
 * prev: false # Disable prev link
 * next: false # Disable prev link
 * ---
 */

/**
 * MyStation module: updateApp
 * @module updateApp
 * @description
 * MyStation module: updateApp
 * ```javascript
 * // Import
 * import updateApp from '#mystation/modules/updateApp'
 * ```
 */

/**
 * updateApp
 * @alias module:updateApp
 * @description updateApp methods: shortcut for AppManager.updateResources() function
 * @return {boolean} See AppManager
 * @vuepress_syntax_block updateApp
 * @vuepress_syntax_desc Access to updateApp method, where `<args>` are AppManager.updateResources parameters
 * @vuepress_syntax_ctx {root}
 * this.updateApp(...<args>)
 * @vuepress_syntax_ctx {apps}
 * __MYSTATION__.updateApp(...<args>)
 * @vuepress_syntax_ctx {any}
 * window.__MYSTATION_VUE_INSTANCE__.updateApp(...<args>)
 */
export default function updateApp (...args) {
  return this.$AppManager.updateResources(...args)
}

/**
 * @vuepress
 * ---
 * title: hasInstalledApp
 * headline: "MyStation module: hasInstalledApp"
 * sidebarDepth: 0 # To disable auto sidebar links
 * prev: false # Disable prev link
 * next: false # Disable prev link
 * ---
 */

/**
 * MyStation module: hasInstalledApp
 * @module hasInstalledApp
 * @description
 * MyStation module: hasInstalledApp
 * ```javascript
 * // Import
 * import hasInstalledApp from '#mystation/modules/hasInstalledApp'
 * ```
 */

/**
 * hasInstalledApp
 * @alias module:hasInstalledApp
 * @description hasInstalledApp method: shortcut for store getter 'Apps/getApp'
 * @param {string} appName - The app unique name
 * @return {(App|boolean)} - See store/Apps
 * @vuepress_syntax_block hasInstalledApp
 * @vuepress_syntax_desc Access to hasInstalledApp method, where `<appName>` is the app name
 * @vuepress_syntax_ctx {root}
 * this.hasInstalledApp(<appName>)
 * @vuepress_syntax_ctx {apps}
 * __MYSTATION__.hasInstalledApp(<appName>)
 * @vuepress_syntax_ctx {any}
 * window.__MYSTATION_VUE_INSTANCE__.hasInstalledApp(<appName>)
 */
export default function hasInstalledApp (appName) {
  return this.$store.getters['Apps/getApp'](appName)
}

/**
 * @vuepress
 * ---
 * title: wait
 * headline: "MyStation module: wait"
 * sidebarDepth: 0 # To disable auto sidebar links
 * prev: false # Disable prev link
 * next: false # Disable prev link
 * ---
 */

/**
 * MyStation module: wait
 * @module wait
 * @description
 * MyStation module: wait
 * ```javascript
 * // Import
 * import wait from '#mystation/modules/wait'
 * ```
 */

/**
 * wait
 * @alias module:wait
 * @description wait method: shortcut for LoadingManager.wait()
 * @return {void} See LoadingManager
 * @vuepress_syntax_block wait
 * @vuepress_syntax_desc Access to wait method, where `<args>` are LoadingManager.wait parameters
 * @vuepress_syntax_ctx {root}
 * this.wait(...<args>)
 * @vuepress_syntax_ctx {apps}
 * __MYSTATION__.wait(...<args>)
 * @vuepress_syntax_ctx {any}
 * window.__MYSTATION_VUE_INSTANCE__.wait(...<args>)
 */
export default async function wait (...args) {
  return this.$LoadingManager.wait(...args)
}

/**
 * @vuepress
 * ---
 * title: on
 * headline: "MyStation module: on"
 * sidebarDepth: 0 # To disable auto sidebar links
 * prev: false # Disable prev link
 * next: false # Disable prev link
 * ---
 */

/**
 * MyStation module: on
 * @module on
 * @description
 * MyStation module: on
 * ```javascript
 * // Import
 * import on from '#mystation/modules/on'
 * ```
 */

/**
 * on
 * @alias module:on
 * @description on method: shortcut for EventBus $on method
 * @return {void}
 * @vuepress_syntax_block on
 * @vuepress_syntax_desc Access to on method, where `<args>` are EventBus.$on parameters
 * @vuepress_syntax_ctx {root}
 * this.on(...<args>)
 * @vuepress_syntax_ctx {apps}
 * __MYSTATION__.on(...<args>)
 * @vuepress_syntax_ctx {any}
 * window.__MYSTATION_VUE_INSTANCE__.on(...<args>)
 */
export default function on (...args) {
  return this.$EventBus.$on(...args)
}

/**
 * @vuepress
 * ---
 * title: ui
 * headline: "MyStation module: ui"
 * sidebarDepth: 0 # To disable auto sidebar links
 * prev: false # Disable prev link
 * next: false # Disable prev link
 * ---
 */

/**
 * MyStation module: ui
 * @module ui
 * @description
 * MyStation module: ui
 * ```javascript
 * // Import
 * import ui from '#mystation/modules/ui'
 * ```
 */

/**
 * display
 * @alias module:ui.display
 * @description display method: shortcut for Store.actions.switchMainframe function
 * @param {string} name - The component name to display
 * @return {void}
 * @vuepress_syntax_block ui.display
 * @vuepress_syntax_desc Access to ui.display method, where `<name>` is the component name
 * @vuepress_syntax_ctx {root}
 * this.ui.display(<name>)
 * @vuepress_syntax_ctx {apps}
 * __MYSTATION__.ui.display(<name>)
 * @vuepress_syntax_ctx {any}
 * window.__MYSTATION_VUE_INSTANCE__.ui.display(<name>)
 */
function display (name) {
  return this.$store.dispatch('switchMainframe', name)
}

const ui = {
  display
}

/**
 * ui
 * @alias module:ui
 * @typicalname ui
 * @type {Object}
 * @constant
 * @description ui module
 * @vuepress_syntax_block ui
 * @vuepress_syntax_desc Access to ui module
 * @vuepress_syntax_ctx {root}
 * this.ui
 * @vuepress_syntax_ctx {apps}
 * __MYSTATION__.ui
 * @vuepress_syntax_ctx {any}
 * window.__MYSTATION_VUE_INSTANCE__.ui
 */
export default ui

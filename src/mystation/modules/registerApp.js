/**
 * @vuepress
 * ---
 * title: registerApp
 * headline: "MyStation module: registerApp"
 * sidebarDepth: 0 # To disable auto sidebar links
 * prev: false # Disable prev link
 * next: false # Disable prev link
 * ---
 */

/**
 * MyStation module: registerApp
 * @module registerApp
 * @description
 * MyStation module: registerApp
 * ```javascript
 * // Import
 * import registerApp from '#mystation/modules/registerApp'
 * ```
 */

/**
 * registerApp
 * @alias module:registerApp
 * @description registerApp methods: shortcut for AppManager.register() function
 * @return {boolean} See AppManager
 * @vuepress_syntax_block registerApp
 * @vuepress_syntax_desc Access to registerApp method, where `<args>` are AppManager.register parameters
 * @vuepress_syntax_ctx {root}
 * this.registerApp(...<args>)
 * @vuepress_syntax_ctx {apps}
 * __MYSTATION__.registerApp(...<args>)
 * @vuepress_syntax_ctx {any}
 * window.__MYSTATION_VUE_INSTANCE__.registerApp(...<args>)
 */
export default function registerApp (...args) {
  return this.$AppManager.register(...args)
}

/**
 * @vuepress
 * ---
 * title: isAppInitialized
 * headline: "MyStation module: isAppInitialized"
 * sidebarDepth: 0 # To disable auto sidebar links
 * prev: false # Disable prev link
 * next: false # Disable prev link
 * ---
 */

/**
 * MyStation module: isAppInitialized
 * @module isAppInitialized
 * @description
 * MyStation module: isAppInitialized
 * ```javascript
 * // Import
 * import isAppInitialized from '#mystation/modules/isAppInitialized'
 * ```
 */

/**
 * isAppInitialized
 * @alias module:isAppInitialized
 * @description isAppInitialized method: shortcut for store current user apps => is_initialized
 * @param {string} appName
 * @return {boolean}
 * @vuepress_syntax_block isAppInitialized
 * @vuepress_syntax_desc Access to isAppInitialized method, where `<appName>` is the app name
 * @vuepress_syntax_ctx {root}
 * this.isAppInitialized(<appName>)
 * @vuepress_syntax_ctx {apps}
 * __MYSTATION__.isAppInitialized(<appName>)
 * @vuepress_syntax_ctx {any}
 * window.__MYSTATION_VUE_INSTANCE__.isAppInitialized(<appName>)
 */
export default function isAppInitialized (appName) {
  const userApp = this.$store.state.Users.currentUser.getApp(appName)
  return Boolean(userApp.appConfig.is_initialized)
}

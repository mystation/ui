/**
 * @vuepress
 * ---
 * title: network
 * headline: "MyStation module: network"
 * sidebarDepth: 0 # To disable auto sidebar links
 * prev: false # Disable prev link
 * next: false # Disable prev link
 * ---
 */

/**
 * MyStation module: network
 * @module network
 * @description
 * MyStation module: network
 * ```javascript
 * // Import
 * import network from '#mystation/modules/network'
 * ```
 */

/**
 * check
 * @alias module:network.check
 * @description check method: shortcut for Store.actions.checkHttp action (same parameters).
 * @return {(boolean|Error)} See store
 * @vuepress_syntax_block network.check
 * @vuepress_syntax_desc Access to network.check method, where `<args>` are Store.actions.checkHttp parameters
 * @vuepress_syntax_ctx {root}
 * this.network.check(...<args>)
 * @vuepress_syntax_ctx {apps}
 * __MYSTATION__.network.check(...<args>)
 * @vuepress_syntax_ctx {any}
 * window.__MYSTATION_VUE_INSTANCE__.network.check(...<args>)
 */
function check (...args) {
  return this.$store.dispatch('checkHttp', ...args)
}

// Object module
const network = {
  check
}

/**
 * network
 * @alias module:network
 * @typicalname network
 * @type {Object}
 * @constant
 * @description network module
 * @vuepress_syntax_block network
 * @vuepress_syntax_desc Access to network module
 * @vuepress_syntax_ctx {root}
 * this.network
 * @vuepress_syntax_ctx {apps}
 * __MYSTATION__.network
 * @vuepress_syntax_ctx {any}
 * window.__MYSTATION_VUE_INSTANCE__.network
 */
export default network

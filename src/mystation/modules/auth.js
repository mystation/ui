/**
 * @vuepress
 * ---
 * title: auth
 * headline: "MyStation module: auth"
 * sidebarDepth: 0 # To disable auto sidebar links
 * prev: false # Disable prev link
 * next: false # Disable prev link
 * ---
 */

/**
 * MyStation module: auth
 * @module auth
 * @description
 * MyStation module: auth
 * ```javascript
 * // Import
 * import auth from '#mystation/modules/auth'
 * ```
 */

/**
 * check
 * @alias module:auth.check
 * @description check method: shortcut for Store.Users.actions.checkAuthenticated action
 * @return {boolean} See store Users module
 * @vuepress_syntax_block auth.check
 * @vuepress_syntax_desc Access to auth.check method
 * @vuepress_syntax_ctx {root}
 * this.auth.check()
 * @vuepress_syntax_ctx {apps}
 * __MYSTATION__.auth.check()
 * @vuepress_syntax_ctx {any}
 * window.__MYSTATION_VUE_INSTANCE__.auth.check()
 */
function check () {
  return this.$store.dispatch('Users/checkAuthenticated')
}

/**
 * isLoading
 * @alias module:auth.isLoading
 * @description isLoading method: shortcut for Store.state.Users.currentUserLoading
 * @return {boolean} See store Users module
 * @vuepress_syntax_block auth.isLoading
 * @vuepress_syntax_desc Access to auth.isLoading method
 * @vuepress_syntax_ctx {root}
 * this.auth.isLoading()
 * @vuepress_syntax_ctx {apps}
 * __MYSTATION__.auth.isLoading()
 * @vuepress_syntax_ctx {any}
 * window.__MYSTATION_VUE_INSTANCE__.auth.isLoading()
 */
function isLoading () {
  return this.$store.state.Users.currentUserLoading
}

/**
 * login
 * @alias module:auth.login
 * @description login method: shortcut for Store.Users.actions.login (same parameters).
 * @return {(boolean|Error)} See store Users module
 * @vuepress_syntax_block auth.login
 * @vuepress_syntax_desc Access to auth.login method, where `<args>` are Store.Users.actions.login parameters
 * @vuepress_syntax_ctx {root}
 * this.auth.login(...<args>)
 * @vuepress_syntax_ctx {apps}
 * __MYSTATION__.auth.login(...<args>)
 * @vuepress_syntax_ctx {any}
 * window.__MYSTATION_VUE_INSTANCE__.auth.login(...<args>)
 */
function login (...args) {
  return this.$store.dispatch('Users/login', ...args)
}

/**
 * logout
 * @alias module:auth.logout
 * @description logout method
 * @return {void}
 * @vuepress_syntax_block auth.logout
 * @vuepress_syntax_desc Access to auth.logout method
 * @vuepress_syntax_ctx {root}
 * this.auth.logout()
 * @vuepress_syntax_ctx {apps}
 * __MYSTATION__.auth.logout()
 * @vuepress_syntax_ctx {any}
 * window.__MYSTATION_VUE_INSTANCE__.auth.logout()
 */
async function logout () {
  await this.$store.dispatch('Users/logout')
  // Reload page
  window.location.reload()
}

/**
 * user
 * @alias module:auth.user
 * @description user method: shortcut for Store.Users.state.currentUser object
 * @return {User} See store Users module
 * @vuepress_syntax_block auth.user
 * @vuepress_syntax_desc Access to auth.user method
 * @vuepress_syntax_ctx {root}
 * this.auth.user()
 * @vuepress_syntax_ctx {apps}
 * __MYSTATION__.auth.user()
 * @vuepress_syntax_ctx {any}
 * window.__MYSTATION_VUE_INSTANCE__.auth.user()
 */
function user () {
  return this.$store.state.Users.currentUser
}

// Object module
const auth = {
  check,
  isLoading,
  login,
  logout,
  user
}

/**
 * auth
 * @alias module:auth
 * @typicalname auth
 * @type {Object}
 * @constant
 * @description auth module
 * @vuepress_syntax_block auth
 * @vuepress_syntax_desc Access to auth module
 * @vuepress_syntax_ctx {root}
 * this.auth
 * @vuepress_syntax_ctx {apps}
 * __MYSTATION__.auth
 * @vuepress_syntax_ctx {any}
 * window.__MYSTATION_VUE_INSTANCE__.auth
 */
export default auth

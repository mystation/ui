/**
 * @vuepress
 * ---
 * title: generate
 * headline: "MyStation module: generate"
 * sidebarDepth: 0 # To disable auto sidebar links
 * prev: false # Disable prev link
 * next: false # Disable prev link
 * ---
 */

/**
 * MyStation module: generate
 * @module generate
 * @description
 * MyStation module: generate
 * ```javascript
 * // Import
 * import generate from '#mystation/modules/generate'
 * ```
 */

/**
 * appModuleResourceMappers
 * @alias module:generate.appModuleResourceMappers
 * @description appModuleResourceMappers methods: shortcut for AppManager.generateAppModuleResourceMappers() function (same parameters).
 * @return {(Object|boolean)} See AppManager
 * @vuepress_syntax_block generate.appModuleResourceMappers
 * @vuepress_syntax_desc Access to generate.appModuleResourceMappers method, where `<args>` are AppManager.generateAppModuleResourceMappers parameters
 * @vuepress_syntax_ctx {root}
 * this.generate.appModuleResourceMappers(...<args>)
 * @vuepress_syntax_ctx {apps}
 * __MYSTATION__.generate.appModuleResourceMappers(...<args>)
 * @vuepress_syntax_ctx {any}
 * window.__MYSTATION_VUE_INSTANCE__.generate.appModuleResourceMappers(...<args>)
 */
function appModuleResourceMappers (...args) {
  return this.$AppManager.generateAppModuleResourceMappers(...args)
}

const generate = {
  appModuleResourceMappers
}

/**
 * generate
 * @alias module:generate
 * @typicalname generate
 * @type {Object}
 * @constant
 * @description generate module
 * @vuepress_syntax_block generate
 * @vuepress_syntax_desc Access to generate module
 * @vuepress_syntax_ctx {root}
 * this.generate
 * @vuepress_syntax_ctx {apps}
 * __MYSTATION__.generate
 * @vuepress_syntax_ctx {any}
 * window.__MYSTATION_VUE_INSTANCE__.generate
 */
export default generate

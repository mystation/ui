/**
 * @vuepress
 * ---
 * title: initializeApp
 * headline: "MyStation module: initializeApp"
 * sidebarDepth: 0 # To disable auto sidebar links
 * prev: false # Disable prev link
 * next: false # Disable prev link
 * ---
 */

/**
 * MyStation module: initializeApp
 * @module initializeApp
 * @description
 * MyStation module: initializeApp
 * ```javascript
 * // Import
 * import initializeApp from '#mystation/modules/initializeApp'
 * ```
 */

/**
 * initializeApp
 * @alias module:initializeApp
 * @description initializeApp method: shortcut for store action 'Users/initializeApp'
 * @param {string} appName - The app unique name
 * @return {(Object|boolean)} See store/Users
 * @vuepress_syntax_block initializeApp
 * @vuepress_syntax_desc Access to initializeApp method, where `<appName>` is the app name
 * @vuepress_syntax_ctx {root}
 * this.initializeApp(<appName>)
 * @vuepress_syntax_ctx {apps}
 * __MYSTATION__.initializeApp(<appName>)
 * @vuepress_syntax_ctx {any}
 * window.__MYSTATION_VUE_INSTANCE__.initializeApp(<appName>)
 */
export default async function initializeApp (appName) {
  const response = await this.$store.dispatch('Users/initializeApp', appName)
  return response
}

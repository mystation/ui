/**
 * @vuepress
 * ---
 * title: log
 * headline: "MyStation module: log"
 * sidebarDepth: 0 # To disable auto sidebar links
 * prev: false # Disable prev link
 * next: false # Disable prev link
 * ---
 */

/**
 * MyStation module: log
 * @module log
 * @description
 * MyStation module: log
 * ```javascript
 * // Import
 * import log from '#mystation/modules/log'
 * ```
 */

/**
 * log
 * @alias module:log
 * @description log method: shortcut for Logger.push()
 * @return {void}
 * @vuepress_syntax_block log
 * @vuepress_syntax_desc Access to log method, where `<args>` are Logger.push parameters
 * @vuepress_syntax_ctx {root}
 * this.log(...<args>)
 * @vuepress_syntax_ctx {apps}
 * __MYSTATION__.log(...<args>)
 * @vuepress_syntax_ctx {any}
 * window.__MYSTATION_VUE_INSTANCE__.log(...<args>)
 */
export default function log (...args) {
  this.$Logger.push(...args) // Simple call...
}

/**
 * @vuepress
 * ---
 * title: ErrorManager
 * headline: "MyStation service: ErrorManager"
 * prev: false # Disable prev link
 * next: false # Disable prev link
 * ---
 */

/**
 * MyStation service: ErrorManager
 * @module error-manager
 * @description
 * MyStation service: ErrorManager
 * ```javascript
 * // Import
 * import ErrorManager from '#mystation/services/error-manager'
 * ```
 */

import Vue from 'vue'
// Classes
import ErrorManager from './ErrorManager'
// Custom error references by language
/* eslint-disable camelcase */
import errors_en from '@/i18n/errors/en'
import errors_fr from '@/i18n/errors/fr'
/* eslint-enable camelcase */

Vue.use(ErrorManager)

// Options for ErrorManager
const options = {
  i18n: true // Activate i18n management
}

/**
 * ErrorManager
 * @alias module:error-manager
 * @type {ErrorManager}
 * @constant
 * @description ErrorManager service
 * See also [ErrorManager class]{@link ErrorManager}
 * @vuepress_syntax_block ErrorManager
 * @vuepress_syntax_desc Access to ErrorManager instance
 * @vuepress_syntax_ctx {root}
 * this.$ErrorManager
 * @vuepress_syntax_ctx {apps}
 * this.$ErrorManager
 * __MYSTATION__.$ErrorManager
 * @vuepress_syntax_ctx {any}
 * window.__MYSTATION_VUE_INSTANCE__.$ErrorManager
 */
export default new ErrorManager({
  errorRefs: {
    en: errors_en,
    fr: errors_fr
  },
  options
})

/**
 * @vuepress
 * ---
 * title: ErrorManager class
 * headline: "MyStation ErrorManager class"
 * sidebarDepth: 0 # To disable auto sidebar links
 * prev: false # Disable prev link
 * next: false # Disable prev link
 * ---
 */

// Classes
import ServiceAsPlugin from '#mystation/services/ServiceAsPlugin.js'

/**
 * ErrorManager class
 * @typicalname errorManager
 * @extends ServiceAsPlugin
 * @classdesc
 * MyStation Error Manager: ErrorManager class.
 * See also [ServiceAsPlugin]{@link ../ServiceAsPlugin}
 * ```javascript
 * // Import
 * import ErrorManager from '#mystation/services/error-manager/ErrorManager'
 * ```
 */
class ErrorManager extends ServiceAsPlugin {
  /**
   * Create a new Error Manager
   * @description Receive an object like:
   * ```javascript
   * // in default case
   * {
   *   errorRefs: {
   *     e0001: {
   *       message: '',
   *       description: ''
   *     }
   *   }
   * }
   * // in case of i18n activated
   * {
   *   errorRefs: {
   *     en: {
   *       object (see above)
   *       ...
   *     },
   *     fr: {
   *       ...
   *     }
   *   },
   *   i18n: true
   * }
   * ```
   * @param {Object} [config = {}] - The error manager config
   * @param {Object} [config.errorRefs] - The error manager error references (objects)
   * @param {Object} [config.options] - Error manager options
   * @param {boolean} [config.options.i18n] - I18n support options
   */
  constructor (config = {}) {
    let errorRefs = {}
    const options = config.options || ErrorManager.defaultOptions()

    if (options.i18n) {
      errorRefs = ErrorManager.mergeI18nErrorRefs(config.errorRefs)
    } else {
      errorRefs = config.errorRefs
    }
    const initVMData = {
      errorRefs: errorRefs,
      error: null,
      history: []
    }
    // Call ServiceAsPlugin constructor
    super(true, initVMData)
  }

  /**
   * errorRefs
   * @category properties
   * @description Get errorRefs
   * @return {Object}
   */
  get errorRefs () {
    return this._vm.errorRefs
  }

  set errorRefs (errorRefs) {
    this._vm.errorRefs = errorRefs
    return this._vm.errorRefs
  }

  /**
   * error
   * @category properties
   * @description When set error, push the error in history
   * @return {Object}
   */
  get error () {
    return this._vm.error
  }

  set error (error) {
    this._vm.$set(this._vm, 'error', error)
    // Prevent for clear current error
    if (error) {
      this.pushInHistory(error)
    }
    return this._vm.last
  }

  /**
   * last
   * @category properties
   * @readonly
   * @description Return last error in history
   * @return {Object}
   */
  get last () {
    return this._vm.history.pop() // Extract last item in an array
  }

  /**
   * history
   * @category properties
   * @readonly
   * @description Return the history
   * @return {Array}
   */
  get history () {
    return this._vm.history
  }

  /**
   * Throw an error that is defined in errorRefs
   * @category methods
   * @param {string} errorId - Error id defined in errorRefs
   * @param {Object} options
   * @return {Object}
   */
  throw (errorId, options = {}) {
    if (this.errorRefs[errorId]) {
      this.error = {
        errorId,
        ...this.errorRefs[errorId],
        options
      }
      return this.last
    } else {
      /* eslint-disable-next-line */
      console.log('Cannot throw an undefined error: "' + errorId + '" is not defined in errorRefs')
    }
  }

  /**
   * pushInHistory
   * @category methods
   * @description Push error in history
   * @param {Object} - The error object
   * @return {Object}
   */
  pushInHistory (error) {
    this._vm.history.push(error)
    return this._vm.last
  }

  /**
   * clear
   * @category methods
   * @description Clear current error (set to null)
   * @return {void}
   */
  clear () {
    this._vm.error = null
  }

  /**
   * clearHistory
   * @category methods
   * @description Clear history
   * @return {void}
   */
  clearHistory () {
    this._vm.history = []
  }

  /**
   * defaultOptions
   * @static
   * @description Return default options for the constructor
   * @return {Object}
   */
  static defaultOptions () {
    return {
      errorRefs: {},
      i18n: false
    }
  }

  /**
   * mergeI18nErrorRefs
   * @description Merge i18n error references
   * @param {Object} errorRefs
   * @return {Object}
   */
  static mergeI18nErrorRefs (errorRefs) {
    const langs = Object.keys(errorRefs)

    const mergedErrorRefs = {}

    // Return a translated object
    var translateObject = (errorObject, lang, output = {}) => {
      Object.keys(errorObject).forEach((prop) => {
        // Check and assign property if it doesn't exist
        if (!output[prop]) {
          output[prop] = {}
        }
        if (typeof errorObject[prop] === 'object') {
          // If object -> recursive
          output[prop] = translateObject(errorObject[prop], lang, output[prop])
        } else if (typeof errorObject[prop] === 'string') {
          // If string, create a prorperty named by 'lang' with the string value
          output[prop][lang] = errorObject[prop]
        } else {
          /* eslint-disable-next-line */
          console.log('ErrorManager: Cannot translate (only string type is authorized) -> lang: "' + lang + '", property: "' + prop + '".')
          return false
        }
      })
      return output
    }

    if (langs.length > 0) {
      // First loop on error IDs for the first lang (ex: 'e0001', 'e0002')
      Object.keys(errorRefs[langs[0]]).forEach((errorId) => {
        // Assign property with errorId (ex: { e0001: {} })
        mergedErrorRefs[errorId] = {}
        // Check if errorId exist in the other languages
        langs.forEach((l) => {
          if (errorRefs[l][errorId]) {
            // Check if all properties values are string
            const translatedError = translateObject(errorRefs[l][errorId], l, mergedErrorRefs[errorId])
            if (translatedError) {
              mergedErrorRefs[errorId] = translatedError
            } else {
              /* eslint-disable-next-line */
              console.log('ErrorManager: Cannot translate object "' + errorId + '" -> locale "' + l + '".')
            }
          } else {
            /* eslint-disable-next-line */
            console.log('ErrorManager: Cannot find error "' + errorId + '" -> locale "' + l + '".')
          }
        })
      })
    }
    return mergedErrorRefs
  }

  /**
   * Install method for Vue
   * The service used as a plugin must absolutely overwrites this static method
   * @ignore
   * @param {Vue} _Vue - Vue
   * @param {Object} [options={}] - The options of the plugin
   */
  static install (_Vue, options = {}) {
    const Vue = _Vue

    const pluginName = 'ErrorManager'

    // Create a 'reactive' mixin
    Vue.mixin(ServiceAsPlugin.createMixin(true, pluginName))

    // Define the getter on the 'private' property
    Object.defineProperty(Vue.prototype, '$' + pluginName, {
      get () { return this['_' + pluginName] }
    })
  }
}

export default ErrorManager

/**
 * @vuepress
 * ---
 * title: Helpers
 * headline: "MyStation service: Helpers"
 * prev: false # Disable prev link
 * next: false # Disable prev link
 * ---
 */

/**
 * MyStation service: Helpers
 * @module helpers
 * @description
 * MyStation service: Helpers
 * ```javascript
 * // Import
 * import Helpers from '#mystation/services/helpers'
 * ```
 */

import Vue from 'vue'
// Classes
import Helpers from './Helpers'

Vue.use(Helpers)

/**
 * Helpers
 * @alias module:helpers
 * @type {Helpers}
 * @constant
 * @description Helpers service
 * See also [Helpers class]{@link Helpers}
 * @vuepress_syntax_block Helpers
 * @vuepress_syntax_desc Access to Helpers instance
 * @vuepress_syntax_ctx {root}
 * this.$Helpers
 * this.$h // alias
 * @vuepress_syntax_ctx {apps}
 * this.$Helpers
 * this.$h // alias
 * __MYSTATION__.$Helpers
 * __MYSTATION__.$h // alias
 * @vuepress_syntax_ctx {any}
 * window.__MYSTATION_VUE_INSTANCE__.$Helpers
 * window.__MYSTATION_VUE_INSTANCE__.$h // alias
 */
export default Helpers

/**
 * @vuepress
 * ---
 * title: Helpers class
 * headline: "MyStation Helpers class"
 * sidebarDepth: 0 # To disable auto sidebar links
 * prev: false # Disable prev link
 * next: false # Disable prev link
 * ---
 */

// Classes
import ServiceAsPlugin from '../ServiceAsPlugin.js'
// Dependencies
import camelize from 'lodash.camelcase'
import capitalize from 'lodash.capitalize'
import kebabcase from 'lodash.kebabcase'
import maxBy from 'lodash.maxby'
import minBy from 'lodash.minby'
import snakecase from 'lodash.snakecase'
import uppercase from 'lodash.uppercase'

/**
 * Helpers class
 * @typicalname helpers
 * @extends ServiceAsPlugin
 * @classdesc
 * MyStation helpers: Helpers class.
 * See also [ServiceAsPlugin]{@link ../ServiceAsPlugin}
 * ```javascript
 * // Import
 * import Helpers from '#mystation/services/helpers/Helpers'
 * ```
 */
class Helpers extends ServiceAsPlugin {
  /**
   * Create a new instance
   */
  constructor () { // eslint-disable-line no-useless-constructor
    // Call ServiceAsPlugin constructor
    super()
  }

  /**
   * camelize
   * @description Camelize a string (lodash.camelize).
   * Defined as Vue filter.
   * @example
   * Helpers.camelize('Foo Bar') // => 'fooBar'
   * {{ 'foo' | camelize }} // Vue filter
   * @param {string} str - The string to camelize
   * @return {string}
   */
  static camelize (str) {
    return camelize(str)
  }

  /**
   * capitalize
   * @description Capitalize the first letter of a string (lodash.capitalize).
   * Defined as Vue filter.
   * @example
   * Helpers.capitalize('Foo Bar') // => 'Foo bar'
   * {{ 'foo' | capitalize }} // Vue filter
   * @param {string} str - The string to capitalize
   * @return {string}
   */
  static capitalize (str) {
    return capitalize(str)
  }

  /**
   * isValidHttpUrl
   * @description Return new URL object if str is a valid http url
   * @example
   * Helpers.isValidHttpUrl('http://example.com/') // success: return new URL('http://example.com/')
   * Helpers.isValidHttpUrl('example.com') // fail
   * @param {string} str - The url string
   * @return {(URL|boolean)}
   */
  static isValidHttpUrl (str) {
    let url
    try {
      url = new URL(str)
    } catch (error) {
      return false
    }
    if (url.protocol === 'http:' || url.protocol === 'https:') {
      return url
    } else {
      return false
    }
  }

  /**
   * getHostHttpUrl
   * @description Return server url
   * @example
   * Helpers.getHostHttpUrl('http://example.com/blog/posts?id=1') // return 'http://example.com/'
   * Helpers.getHostHttpUrl('example.com/blog/posts/') // fail
   * @param {string} str - The url string
   * @return {(string|boolean)}
   */
  static getHostHttpUrl (str) {
    let url
    try {
      url = new URL(str)
    } catch (error) {
      return false
    }
    if (url.protocol === 'http:' || url.protocol === 'https:') {
      return url.protocol + '//' + url.host
    } else {
      return false
    }
  }

  /**
   * kebabcase
   * @description Transform a full string to kebabcase (lodash.kebabcase).
   * Defined as Vue filter.
   * @example
   * Helpers.kebabcase('Foo Bar') // => 'foo-bar'
   * {{ 'foo' | kebabcase }} // Vue filter
   * @param {string} str - The string to kebabcase
   * @return {string}
   */
  static kebabcase (str) {
    return kebabcase(str)
  }

  /**
   * maxBy
   * @description Find max by in array (lodash.maxby).
   * @example
   * const array = [ { id: 1, name: 'John' }, { id: 2, name: 'Jane' } ]
   * Helpers.maxBy(array, function (item) { return item.id }) // Return -> { id: 2, name: 'Jane' }
   * Helpers.maxBy(array, function (item) { return item.name }) // Return -> { id: 1, name: 'John' }
   * @param {Array} array - The array
   * @param {Function} sortFct - The sort function
   * @return {string}
   */
  static maxBy (array, sortFct) {
    return maxBy(array, sortFct)
  }

  /**
   * minBy
   * @description Find min by in array (lodash.minby).
   * @example
   * const array = [ { id: 1, name: 'John' }, { id: 2, name: 'Jane' } ]
   * Helpers.minBy(array, function (item) { return item.id }) // Return -> { id: 1, name: 'John' }
   * Helpers.minBy(array, function (item) { return item.name }) // Return -> { id: 2, name: 'Jane' }
   * @param {Array} array - The array
   * @param {Function} sortFct - The sort function
   * @return {string}
   */
  static minBy (array, sortFct) {
    return minBy(array, sortFct)
  }

  /**
   * snakecase
   * @description Transform a full string to snakecase (lodash.snakecase).
   * Defined as Vue filter.
   * @example
   * Helpers.snakecase('Foo Bar') // => 'foo_bar'
   * {{ 'foo' | snakecase }} // Vue filter
   * @param {string} str - The string to snakecase
   * @return {string}
   */
  static snakecase (str) {
    return snakecase(str)
  }

  /**
   * uppercase
   * @description Transform a full string to uppercases (lodash.uppercase).
   * Defined as Vue filter.
   * @example
   * Helpers.uppercase('Foo Bar') // => 'FOO BAR'
   * {{ 'foo' | uppercase }} // Vue filter
   * @param {string} str - The string to uppercase
   * @return {string}
   */
  static uppercase (str) {
    return uppercase(str)
  }

  /**
   * Install method for Vue
   * The service used as a plugin must absolutely overwrites this static method
   * @ignore
   * @param {Vue} _Vue - The Vue instance
   * @param {Object} [options={}] - The options of the plugin
   */
  static install (_Vue, options = {}) {
    const Vue = _Vue
    // Declare filters
    Vue.filter('camelize', Helpers.camelize)
    Vue.filter('capitalize', Helpers.capitalize)
    Vue.filter('kebabcase', Helpers.kebabcase)
    Vue.filter('snakecase', Helpers.snakecase)
    Vue.filter('uppercase', Helpers.uppercase)
    // Define '$h' for each Vue instance
    Vue.prototype.$h = Helpers
  }
}

export default Helpers

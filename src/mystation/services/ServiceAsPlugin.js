/**
 * @vuepress
 * ---
 * title: ServiceAsPlugin class
 * headline: "MyStation ServiceAsPlugin class"
 * sidebarDepth: 0 # To disable auto sidebar links
 * ---
 */

import Vue from 'vue'

/**
 * ServiceAsPlugin class
 * @abstract
 * @typicalname service
 * @classdesc
 * MyStation services: ServiceAsPlugin abstract class.
 * A parent class to declare a service as a Vue plugin
 * ```javascript
 * // Import
 * import ServiceAsPlugin from '#mystation/services/ServiceAsPlugin'
 * ```
 */
class ServiceAsPlugin {
  /**
   * @private
   */
  _vm

  /**
   * ServiceAsPlugin abstract class cannot be instantiated with 'new'
   * @param {boolean} [reactive=false] - Define if the plugin must be 'reactive'.
   * if true, init VM with vmData argument
   * @param {Object} [vmData={}] - The data to send to _initVM method for reactivity
   */
  constructor (reactive = false, vmData = {}) {
    // Needed for abstract class
    if (this.constructor === ServiceAsPlugin) {
      throw new TypeError('Abstract class "ServiceAsPlugin" cannot be instantiated directly')
    } else {
      this._vm = null // Default to null
      // If reactive
      if (reactive) {
        // Init vm to use as a 'reactive' plugin
        this._initVM(vmData) // Init vm with received data
      }
    }
  }

  /**
   * vm
   * @readonly
   * @category properties
   * @return {Vue} Returns the Vue Instance
   */
  get vm () {
    return this._vm
  }

  /**
   * setVMProp
   * @description Set a property to this._vm
   * @category methods
   * @param {String} name - Name of the property
   * @param {any} value - The value
   * @return {void}
   */
  setVMProp (name, value) {
    this._vm.$set(this._vm, name, value)
  }

  /**
   * _initVM
   * @category methods
   * @description Init the view model to use with Vue
   * @param {Object} data
   */
  _initVM (data) {
    if (this._vm) {
      return
    }
    // Save the initial state of silent config
    const silent = Vue.config.silent
    Vue.config.silent = true
    // Assign a new Vue instance with data to this._vm
    this._vm = new Vue({ data })
    // Re-apply silent config
    Vue.config.silent = silent
  }

  /**
   * destroyVM
   * @category methods
   * @description Destroy the vm
   * @return {void}
   */
  destroyVM () {
    this._vm.$destroy()
  }

  /**
   * createMixin
   * @static
   * @description Create a mixin to apply to Vue
   * @param {boolean} [reactive=false] - Define if the plugin must be 'reactive'
   * If set to true, it will inject 'beforeCreate' and 'beforeDestroy' method
   * that will manage the inheritance for the reactivity using the _vm property
   * and _initVM() method of the instance
   * @param {string} [pluginName=null] - The plugin name, required if reactive is set to true
   * @param {Object} [customMixin={}] - An Object that follows the same schema as a Vue mixin,
   * received by the user if he needs to customize the mixin. Note that 'beforeCreate' and 'beforeDestroy'
   * methods declared below will be overwritten if the user provide these methods into the customMixin object param
   * @return {Object} Return the created mixin
   */
  static createMixin (reactive = false, pluginName = null, customMixin = {}) {
    const mixin = {
      beforeCreate: null,
      beforeDestroy: null
    }
    // If reactive set to true, auto-set the 'beforeCreate' and
    // 'beforeDestroy' method that will be called to make the
    // plugin 'reactive'
    if (reactive) {
      // Check if plugin name is provided
      if (pluginName && typeof pluginName === 'string') {
        // Default ServiceAsPlugin 'beforeCreate' mixin hook (reactive)
        mixin.beforeCreate = function () {
          // Get Vue instance options
          const options = this.$options
          // Check if [pluginName] is defined for the instance
          options[pluginName] = options[pluginName] || null
          // If [pluginName] is defined and instance of ServiceAsPlugin
          if (options[pluginName]) {
            if (options[pluginName] instanceof ServiceAsPlugin) {
              // Assign to 'private' _[pluginName]
              this['_' + pluginName] = options[pluginName]
            } else {
              /* eslint-disable-next-line */
              console.log(`ServiceAsPlugin mixin : ${pluginName} plugin mixin (beforeCreate hook): Cannot be interpreted '${pluginName}' option.`)
            }
          // Else, check the root Vue instance
          } else if (this.$root && this.$root['$' + pluginName] && this.$root['$' + pluginName] instanceof ServiceAsPlugin) {
            this['_' + pluginName] = this.$root['$' + pluginName]
          // Else, check the parent Vue instance
          } else if (options.parent && options.parent['$' + pluginName] && options.parent['$' + pluginName] instanceof ServiceAsPlugin) {
            this['_' + pluginName] = options.parent['$' + pluginName]
          }
        }
        // Default ServiceAsPlugin 'beforeDestroy' mixin hook (reactive)
        mixin.beforeDestroy = function () {
          if (!this['_' + pluginName]) { return }
          this['_' + pluginName] = null
        }
      } else {
        throw new TypeError('ServiceAsPlugin class : if reactive plugin, second parameter "pluginName" must be provided as a String.')
      }
    }
    // Mix with customMixin
    // -> If user define a beforeCreate method in the child class,
    // it will overwrites the beforeCreate method declared above
    // Merge objects
    Object.assign(mixin, customMixin) // customMixin overwrites mixin !!
    // Return the mixin
    return mixin
  }

  /**
   * install
   * @static
   * @abstract
   * @description install method for Vue plugin: must be implemented.
   * @param {Vue} _Vue - The Vue instance
   * @param {Object} [options={}] - The options of the plugin
   */
  static install (_Vue, options = {}) {
    throw new Error('ServiceAsPlugin class : you must implement the "install" method')
  }
}

export default ServiceAsPlugin

/**
 * @vuepress
 * ---
 * title: APIManager
 * headline: "MyStation service: APIManager"
 * prev: false # Disable prev link
 * next: false # Disable prev link
 * ---
 */

/**
 * MyStation service: APIManager
 * @module api-manager
 * @description
 * MyStation service: APIManager
 * ```javascript
 * // Import
 * import APIManager from '#mystation/services/api-manager'
 * ```
 */

import Vue from 'vue'
// Classes
import API from './API'
import APIOptions from './APIOptions'
import APIManager from './APIManager'
// API configurations
import CONFIG from '@/config/config.js'

const APIConfigs = CONFIG.API
// Init collection
const APICollection = [] // Array of APIOptions

// Loop on API configurations
Object.keys(APIConfigs).forEach((apiName) => {
  // Check if development mode and if dev url is defined
  if (process.env.NODE_ENV === 'development') {
    if (CONFIG.DEVELOPMENT.API[apiName]) {
      APIConfigs[apiName].baseURL = CONFIG.DEVELOPMENT.API[apiName].APIBaseURL
    }
  }
  // Create a new APIOptions
  const options = new APIOptions(apiName, 'axios', APIConfigs[apiName])
  // Create a new API
  const api = new API(options)
  // Push to collection
  APICollection.push(api)
})

Vue.use(APIManager)

/**
 * APIManager
 * @alias module:api-manager
 * @type {APIManager}
 * @constant
 * @description APIManager service initialized with default APIs (see configuration).
 * See also [APIManager class]{@link APIManager}
 * @vuepress_syntax_block APIManager
 * @vuepress_syntax_desc Access to APIManager instance
 * @vuepress_syntax_ctx {root}
 * this.$APIManager
 * @vuepress_syntax_ctx {apps}
 * this.$APIManager
 * __MYSTATION__.$APIManager
 * @vuepress_syntax_ctx {any}
 * window.__MYSTATION_VUE_INSTANCE__.$APIManager
 */
export default new APIManager(APICollection)

/**
 * @vuepress
 * ---
 * title: EventBus
 * headline: "MyStation service: EventBus"
 * sidebarDepth: 0 # To disable auto sidebar links
 * prev: false # Disable prev link
 * next: false # Disable prev link
 * ---
 */

/**
 * MyStation service: EventBus
 * @module event-bus
 * @description
 * MyStation service: EventBus
 * ```javascript
 * // Import
 * import EventBus from '#mystation/services/event-bus'
 * ```
 */

import Vue from 'vue'

const EventBus = new Vue()

Vue.prototype.$EventBus = EventBus

/**
 * EventBus
 * @alias module:event-bus
 * @type {Vue}
 * @constant
 * @description EventBus service
 * The EventBus is the global event bus of MyStation system.
 * All Vue instances have access to this.$EventBus and its properties:
 * - $on
 * - $emit
 * - ...
 * @vuepress_syntax_block EventBus
 * @vuepress_syntax_desc Access to EventBus instance
 * @vuepress_syntax_ctx {root}
 * this.$EventBus
 * @vuepress_syntax_ctx {apps}
 * this.$EventBus
 * __MYSTATION__.$EventBus
 * @vuepress_syntax_ctx {any}
 * window.__MYSTATION_VUE_INSTANCE__.$EventBus
 */
export default EventBus

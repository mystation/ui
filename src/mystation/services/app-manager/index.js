/**
 * @vuepress
 * ---
 * title: AppManager
 * headline: "MyStation service: AppManager"
 * prev: false # Disable prev link
 * next: false # Disable prev link
 * ---
 */

/**
 * MyStation service: AppManager
 * @module app-manager
 * @description
 * MyStation service: AppManager
 * ```javascript
 * // Import
 * import AppManager from '#mystation/services/app-manager'
 * ```
 */

import Vue from 'vue'
import AppManager from './AppManager'

Vue.use(AppManager)

/**
 * AppManager
 * @alias module:app-manager
 * @type {AppManager}
 * @constant
 * @description AppManager service
 * See also [AppManager class]{@link AppManager}
 * @vuepress_syntax_block AppManager
 * @vuepress_syntax_desc Access to AppManager instance
 * @vuepress_syntax_ctx {root}
 * this.$AppManager
 * @vuepress_syntax_ctx {apps}
 * this.$AppManager
 * __MYSTATION__.$AppManager
 * @vuepress_syntax_ctx {any}
 * window.__MYSTATION_VUE_INSTANCE__.$AppManager
 */
export default new AppManager()

/**
 * @vuepress
 * ---
 * title: AppManager class
 * headline: "MyStation AppManager class"
 * sidebarDepth: 0 # To disable auto sidebar links
 * prev: false # Disable prev link
 * next: false # Disable prev link
 * ---
 */

import Vue from 'vue'
import camelize from 'lodash.camelcase'
import uppercase from 'lodash.uppercase'
import snakecase from 'lodash.snakecase'
// Config
import CONFIG from '@/config/config.js'
// Classes
import ServiceAsPlugin from '#mystation/services/ServiceAsPlugin.js'
// Store
import Store from '@/store'
// I18n
import I18n from '@/i18n'
// Error Manager
import ErrorManager from '#mystation/services/error-manager'
// Loading Manager
import LoadingManager from '#mystation/services/loading-manager'
// Event Bus
import EventBus from '#mystation/services/event-bus'
// Helpers
import Helpers from '#mystation/services/helpers'
// API Manager
import APIManager from '#mystation/services/api-manager'
// Logger
import Logger from '#mystation/services/logger'

// Server API
const ServerAPI = APIManager.use('myStationAPI')

/**
 * AppManager class
 * @typicalname appManager
 * @extends ServiceAsPlugin
 * @classdesc
 * MyStation App Manager: AppManager class.
 * See also [ServiceAsPlugin]{@link ../ServiceAsPlugin}
 * ```javascript
 * // Import
 * import AppManager from '#mystation/services/app-manager/AppManager'
 * ```
 */
class AppManager extends ServiceAsPlugin {
  /**
   * Create a new App Manager
   */
  constructor (config = {}) {
    const initVMData = {
      isAppInstalling: false,
      isAppUninstalling: false,
      apps: {}
    }
    // Call ServiceAsPlugin constructor
    super(true, initVMData) // Init as reactive
  }

  /**
   * apps
   * @category properties
   * @return {Object[]}
   */
  get apps () {
    return this._vm.apps
  }

  set apps (apps) {
    this._vm.apps = apps
    return this._vm.apps
  }

  /**
   * isAppInstalling
   * @category properties
   * @return {boolean}
   */
  get isAppInstalling () {
    return this._vm.isAppInstalling
  }

  set isAppInstalling (value) {
    this._vm.isAppInstalling = value
    return this._vm.isAppInstalling
  }

  /**
   * isAppUninstalling
   * @category properties
   * @return {boolean}
   */
  get isAppUninstalling () {
    return this._vm.isAppUninstalling
  }

  set isAppUninstalling (value) {
    this._vm.isAppUninstalling = value
    return this._vm.isAppUninstalling
  }

  /**
   * sync
   * @category methods
   * @description Synchronize apps data with the store.
   * For each app:
   * ```javascript
   * this.apps[app.name] = {
   *   version: app.version,
   *   translations: {
   *     en: { status: false },
   *     fr: { status: false }
   *   },
   *   bundles: {
   *     vendors: {
   *       filename: 'js/vendors.js',
   *       url: '',
   *       element: null,
   *       status: false
   *     },
   *     app: {
   *       filename: 'js/app.js',
   *       url: '',
   *       element: null,
   *       status: false
   *     }
   *   },
   *   components: {},
   *   modules: {}
   * }
   * ```
   * @return {void}
   */
  sync () {
    // Reset this.apps
    this.apps = {}
    Store.state.Apps.apps.forEach((app) => {
      // Initialize an object for each app
      this.apps[app.name] = {
        version: app.version,
        updater: {
          url: '',
          element: null,
          to: app.version,
          from: null,
          tempResources: []
        },
        translations: {
          en: { status: false },
          fr: { status: false }
        },
        bundles: {
          vendors: {
            filename: 'js/vendors.js',
            url: '',
            element: null,
            status: false
          },
          app: {
            filename: 'js/app.js',
            url: '',
            element: null,
            status: false
          }
        },
        components: {},
        modules: {},
        styles: {}
      }
    })
  }

  /**
   * installApp
   * @category methods
   * @description Install an app.
   * Request server API (See `appInstallRequest` method) and
   * reload apps and user data in store.
   * @param {string} appName - App unique name
   * @param {string} appVersion - App version
   * @param {Object} [options] - Install options
   * @param {boolean} [options.update] - For app update
   * @param {boolean} [options.development] - For app in development mode
   * @param {boolean} [options.devAppConfigUrl] - URL of app config for development mode
   * @param {boolean} [options.devAppIconUrl] - URL of app icon for development mode
   * @return {boolean} True if success, else false
   */
  async installApp (appName, appVersion, options = { update: false, development: false, devAppConfigUrl: '', devAppIconUrl: '' }) {
    this.isAppInstalling = true
    let appInstallResponse = await this.appInstallRequest(appName, appVersion, options.update, options.development, options.devAppConfigUrl, options.devAppIconUrl)
    let fromVersion = null
    if (options.update) {
      fromVersion = appInstallResponse.updatedFrom
    }
    // If error
    if (appInstallResponse instanceof Error) {
      Logger.push('error', `Unable to install '${appName}': myStationWebAPI error`, 'console')
      this.isAppInstalling = false
      // Display app installation error
      ErrorManager.throw('e0150', appInstallResponse)
      return false
    } else {
      // Reload apps data (Store)
      await Store.dispatch('Apps/loadApps')
      // Reload user data (for user apps)
      await Store.dispatch('Users/loadCurrentUserData')
      // Synchronization
      this.sync()
      // if update: load updater
      if (options.update) {
        this.apps[appName].updater.tempResources = appInstallResponse.tempResources
        this.apps[appName].updater.from = fromVersion
        await this.loadUpdater(appName)
      }
      // Reload app scripts
      await this.reloadSources()
      // Return success
      this.isAppInstalling = false
      return true
    }
  }

  /**
   * appInstallRequest
   * @category methods
   * @description Ask to API Server to install app.
   * Request `[POST] /apps/install` (See [API Server documentation](/guide/system/api-server/)).
   * @param {string} appName - App unique name
   * @param {string} appVersion - App version
   * @param {boolean} update - For app update
   * @param {boolean} [development = false] - If development mode enabled for the app
   * @param {boolean} [devAppConfigUrl = ''] - URL of app config for development mode
   * @return {(boolean|Error)} Return true if success or Error
   */
  async appInstallRequest (appName, appVersion, update, development = false, devAppConfigUrl = '', devAppIconUrl = '') {
    try {
      const response = await ServerAPI.request(
        'POST',
        '/apps/install',
        {
          data: {
            name: appName,
            version: appVersion,
            update: update,
            development: development,
            dev_app_config_url: devAppConfigUrl,
            dev_app_icon_url: devAppIconUrl
          }
        }
      )
      return response.data
    } catch (error) {
      return error
    }
  }

  /**
   * uninstallApp
   * @category methods
   * @description Uninstall an app.
   * Request `[POST] /apps/install` (See [API Server documentation](/guide/system/api-server/)).
   * If success:
   * - unregister app styles
   * - remove app translations
   * - unregister store modules
   * - remove bundles (scripts)
   * - reload apps and user data in store
   * - synchronize with store data
   * - reload all apps bundles
   * @param {string} appName - The unique app name
   * @return {(boolean|Error)}
   */
  async uninstallApp (appName) {
    this.isAppUninstalling = true
    try {
      await ServerAPI.request(
        'POST',
        '/apps/uninstall',
        {
          data: {
            name: appName
          }
        }
      )
    } catch (error) {
      this.isAppUninstalling = false
      return error
    }
    // Remove styles
    this.unregisterAppStyles(appName)
    // Remove translations
    this.unregisterAppTranslations(appName)
    this.apps[appName].translations.en.status = false
    this.apps[appName].translations.fr.status = false
    // Store modules
    for (let moduleName in this.apps[appName].modules) {
      // Unregister store modules
      this.unregisterAppModule(appName, moduleName)
      this.apps[appName].modules[moduleName] = { status: false }
    }
    // Remove sources (scripts)
    this.unloadSources(appName)
    // Reload apps data
    await Store.dispatch('Apps/loadApps')
    // Reload user data (for user apps)
    await Store.dispatch('Users/loadCurrentUserData')
    // Synchronization
    this.sync()
    // Reload app scripts
    await this.reloadSources()
    this.isAppUninstalling = false
    // Return success
    return true
  }

  /**
   * loadSources
   * @category methods
   * @description Once the instance synchronized (by sync() method),
   * load each app bundles (app.js + vendors.js):
   * add a script tag to DOM with corresponding url
   * @return {void}
   */
  async loadSources () {
    Logger.push('info', 'Apps bundles loading...', 'console')
    // Sources path
    let sourcesPath = '' // Default
    // If local app development enabled
    if (process.env.NODE_ENV === 'development') {
      Logger.push('dev', 'Loading local sources...', 'console')
      sourcesPath = CONFIG.DEVELOPMENT.API.myStationAPI.URL
    }
    // Loop on each app
    for (let appName in this.apps) {
      // If app development enabled for this app
      if (process.env.NODE_ENV === 'development' &&
          CONFIG.DEVELOPMENT.appDevelopment &&
          CONFIG.DEVELOPMENT.appDevelopment[appName]
      ) {
        Logger.push('dev', 'App development mode enabled for "' + appName + '" app. Loading sources from ' + CONFIG.DEVELOPMENT.appDevelopment[appName].URL, 'console')
        this.apps[appName].bundles.vendors.url = CONFIG.DEVELOPMENT.appDevelopment[appName].URL + '/' + this.apps[appName].bundles.vendors.filename
        this.apps[appName].bundles.app.url = CONFIG.DEVELOPMENT.appDevelopment[appName].URL + '/' + this.apps[appName].bundles.app.filename
      } else {
        // Else, adapt to dev or prod url
        this.apps[appName].bundles.vendors.url = sourcesPath + '/storage/apps/' + appName + '/' + this.apps[appName].version + '/' + this.apps[appName].bundles.vendors.filename
        this.apps[appName].bundles.app.url = sourcesPath + '/storage/apps/' + appName + '/' + this.apps[appName].version + '/' + this.apps[appName].bundles.app.filename
      }
      // Load scripts
      for (let bundle in this.apps[appName].bundles) {
        Logger.push('info', `Load '${appName}' app bundle: ${bundle}`, 'console')
        // Create script tag
        const scriptTag = document.createElement('script')
        // Make it synchronous
        scriptTag.async = false
        // Add load event
        scriptTag.onload = () => {
          // Update status
          this.apps[appName].bundles[bundle].status = true
          // Check if all bundles are loaded
          let fullLoaded = true // true by default
          for (let app in this.apps) {
            for (let b in this.apps[app].bundles) {
              if (!this.apps[app].bundles[b].status) {
                fullLoaded = false
              }
            }
          }
          // If all apps bundles loaded
          if (fullLoaded) {
            EventBus.$emit('apps-bundles-loaded-hook')
          }
        }
        // Append to head
        document.head.appendChild(scriptTag)
        // Assign
        this.apps[appName].bundles[bundle].element = scriptTag
        // Apply url as src
        this.apps[appName].bundles[bundle].element.src = this.apps[appName].bundles[bundle].url
      }
    }
    return true
  }

  /**
   * unloadSources
   * @category methods
   * @description Unload app sources (bundles): remove script tag.
   * @param {string} appName - App unique name
   * @return {void}
   */
  unloadSources (appName) {
    // Reset bundles
    for (let bundle in this.apps[appName].bundles) {
      if (this.apps[appName].bundles[bundle].element) {
        // Remove script tag
        Logger.push('info', `Remove '${appName}' app bundle: ${bundle}`, 'console')
        this.apps[appName].bundles[bundle].element.remove()
        // Null
        this.apps[appName].bundles[bundle].element = null
        // Status
        this.apps[appName].bundles[bundle].status = false
      }
    }
  }

  /**
   * reloadSources
   * @category methods
   * @description Reload all apps sources
   * @return {void}
   */
  async reloadSources () {
    Logger.push('info', `Apps bundles reloading...`, 'console')
    for (let appName in this.apps) {
      // Remove app scripts from DOM
      this.unloadSources(appName)
    }
    // Then, reload
    await this.loadSources()
  }

  /**
   * loadUpdater
   * @category methods
   * @description Once the app is updated,
   * load each app updater script (update.js):
   * add a script tag to DOM with corresponding url
   * @param {string} appName - The unique app name
   * @return {void}
   */
  async loadUpdater (appName) {
    Logger.push('info', 'App updater loading...', 'console')
    // Sources path
    let sourcesPath = '' // Default
    // If local app development enabled
    if (process.env.NODE_ENV === 'development') {
      sourcesPath = CONFIG.DEVELOPMENT.API.myStationAPI.URL
    }
    // Else, adapt to dev or prod url
    this.apps[appName].updater.url = sourcesPath + '/storage/apps/' + appName + '/' + this.apps[appName].version + '/js/update.js'
    // Load script
    Logger.push('info', `Load '${appName}' app updater`, 'console')
    // Create script tag
    const scriptTag = document.createElement('script')
    // Make it synchronous
    scriptTag.async = false
    // Append to head
    document.head.appendChild(scriptTag)
    // Assign
    this.apps[appName].updater.element = scriptTag
    // Apply url as src
    this.apps[appName].updater.element.src = this.apps[appName].updater.url
    return true
  }

  /**
   * updateResources
   * @category methods
   * @description Called by app update script.
   * Send to server new resources collections to regenerate tables
   * @param {string} appName - The unique app name
   * @param {Function} modifier - The function that returns the resources to update
   * @return {void}
   */
  async updateResources (appName, modifier) {
    await LoadingManager.wait(Helpers.capitalize(I18n.t('global.loadingDescriptions.appResourcesUpdate')), async () => {
      const oldResources = this.apps[appName].updater.tempResources
      const newResources = modifier(oldResources, this.apps[appName].updater.from)
      try {
        const response = await ServerAPI.request(
          'POST',
          '/apps/updateResources',
          {
            data: {
              name: appName,
              resources: newResources
            }
          }
        )
        return response
      } catch (error) {
        return error
      }
    }, { loading: [ 'mainLoading', 'logoLoading' ] })
    // Remove script tag
    this.apps[appName].updater.element.remove()
  }

  /**
   * register
   * @category methods
   * @description Register an app:
   * - register translations
   * - register components (app component and pancakes)
   * - register store modules
   * This method is called by apps entry point. See [App development documentation](/guide/app-development).
   * @param {string} appName - The unique app name
   * @param {Object} appModule - The app module (!! Not the store module)
   * @param {Object} appModule.components - The app components
   * @param {Object} appModule.components.app - The main app component
   * @param {Object} [appModule.components.pancakes] - The app pancakes
   * @param {Object} appModule.i18n - The app translations
   * @param {Object} appModule.i18n.en - The english app translations
   * @param {Object} [appModule.i18n.fr] - The french app translations
   * @param {Object} [appModule.store] - The app store modules
   * @param {Object} [appModule.store.modules] - The app store modules
   * @return {void}
   */
  async register (appName, appModule) {
    // Apply error handler on window in app script
    Logger.push('info', `Register app : ${appName}`, 'console')
    const translations = appModule.i18n
    const component = appModule.components.app
    const pancakes = appModule.components.pancakes || {}
    const styles = appModule.styles
    // Register app styles
    for (let name in styles) {
      this.apps[appName].styles[name] = {
        status: false,
        url: null,
        element: null
      }
      await this.registerAppStyles(appName, name, styles[name])
      // Declare app pancake component as registered
      this.apps[appName].styles[name].status = true
    }
    // Register app translations
    // First english
    await this.registerAppTranslations(appName, 'en', translations.en)
    // Declare app translation as registered
    this.apps[appName].translations.en.status = true
    // If french translations
    if (translations.fr) {
      this.registerAppTranslations(appName, 'fr', translations.fr)
      // Declare app translation as registered
      this.apps[appName].translations.fr.status = true
    }
    // Register main app component
    await this.registerAppComponent(appName, false, component)
    // Declare app component as registered
    this.apps[appName].components[appName + '-app-component'] = { status: true }
    // // Register pancakes
    for (let name in pancakes) {
      await this.registerAppComponent(appName, name, pancakes[name])
      // Declare app pancake component as registered
      this.apps[appName].components[appName + '-' + name] = { status: true }
    }
    // Register store modules
    if (appModule.store) {
      if (!appModule.store.modules) {
        throw new Error('AppManager can\'t register App. \'store.modules\' property is missing')
      }
      // Loop on modules
      const storeModules = appModule.store.modules
      for (let name in storeModules) {
        await this.registerAppModule(appName, name, storeModules[name])
        // Declare app module as registered
        this.apps[appName].modules[name] = { status: true }
      }
    }
    Logger.push('info', `App '${appName}' registered`, 'console')
  }

  /**
   * registerAppStyles
   * @category methods
   * @description Register app styles (<style> elements)
   * @param {string} appName The app unique name
   * @param {string} name - Style property name
   * @param {string} filename - File name
   * @return {void}
   */
  async registerAppStyles (appName, name, filename) {
    // Sources path
    let stylesPath = '' // Default
    // If local app development enabled
    if (process.env.NODE_ENV === 'development') {
      Logger.push('dev', 'Loading local styles...', 'console')
      stylesPath = CONFIG.DEVELOPMENT.API.myStationAPI.URL
    }
    // If app development enabled for this app: don't need to load styles
    if (process.env.NODE_ENV === 'development' &&
        CONFIG.DEVELOPMENT.appDevelopment &&
        CONFIG.DEVELOPMENT.appDevelopment[appName]
    ) {
      // Css is auto-injected by webpack-dev-server
      Logger.push('dev', `Styles for '${appName}' auto-injected`, 'console')
      // this.apps[appName].styles[name].url = CONFIG.DEVELOPMENT.appDevelopment[appName].URL + '/css/' + filename
    } else {
      // Else, adapt to dev or prod url
      this.apps[appName].styles[name].url = stylesPath + '/storage/apps/' + appName + '/' + this.apps[appName].version + '/css/' + filename
      // Load script
      Logger.push('info', `Load '${appName}' app style: ${filename}`, 'console')
      // Create style tag
      const styleTag = document.createElement('link')
      // Append to head
      document.head.appendChild(styleTag)
      // Assign
      this.apps[appName].styles[name].element = styleTag
      // Apply necessary attributes
      this.apps[appName].styles[name].element.rel = 'stylesheet'
      this.apps[appName].styles[name].element.type = 'text/css'
      this.apps[appName].styles[name].element.media = 'all'
      this.apps[appName].styles[name].element.href = this.apps[appName].styles[name].url
    }
  }

  /**
   * unregisterAppStyles
   * @category methods
   * @description Unregister app styles: remove link tag.
   * @param {string} appName - App unique name
   * @return {void}
   */
  unregisterAppStyles (appName) {
    // Reset styles
    for (let name in this.apps[appName].styles) {
      if (this.apps[appName].styles[name].element) {
        // Remove script tag
        Logger.push('info', `Remove '${appName}' app style: ${name}`, 'console')
        this.apps[appName].styles[name].element.remove()
        // Null
        this.apps[appName].styles[name].element = null
        // Status
        this.apps[appName].styles[name].status = false
      }
    }
  }

  /**
   * registerAppTranslations
   * @category methods
   * @description Register app translations:
   * merge app translations with I18n locale messages
   * @param {string} appName The app unique name
   * @param {string} lang The lang ('fr'/'en')
   * @param {object} messages The messages object
   * @return {void}
   */
  async registerAppTranslations (appName, lang, messages) {
    Logger.push('info', `Register '${appName}' translation: '${lang}'`, 'console')
    I18n.mergeLocaleMessage(lang, {
      apps: {
        [appName]: messages
      }
    })
    return true
  }

  /**
   * unregisterAppTranslations
   * @category methods
   * @description Unregister app translations
   * @param {string} appName - The app unique name
   * @return {void}
   */
  async unregisterAppTranslations (appName) {
    Logger.push('info', `Unregister '${appName}' translations`, 'console')
    // Reset locale messages for each lang after reorganzie
    const en = I18n.messages.en
    const fr = I18n.messages.fr
    delete en.apps[appName]
    delete fr.apps[appName]
    // mergeLocaleMessage not used because we need to 'clear'/remove the property name (appName)
    I18n.setLocaleMessage('en', en)
    I18n.setLocaleMessage('fr', fr)
    return true
  }

  /**
   * registerAppComponent
   * @category methods
   * @description Register an app component. Apply a mixin
   * that checks component options:
   * ```javascript
   * // Component
   * {
   *   // ... other Vue component properties
   *   state: {
   *     '<moduleName>': [] // list of module state properties to map
   *   },
   *   getters: {
   *     '<moduleName>': [] // list of module getters to map
   *   },
   *   actions: {
   *     '<moduleName>': [] // list of module actions to map
   *   }
   * }
   * ```
   * @param {string} appName - The app unique name
   * @param {boolean|string} componentName - The component name. False if app main component
   * @param {Object} component - The component object
   * @return {void}
   */
  async registerAppComponent (appName, componentName, component) {
    // Component name
    // If !componentName: app main component, else: pancake
    componentName = componentName ? appName + '-' + componentName : appName + '-app-component'
    // Apply name
    component.name = componentName
    // Declare the component globally
    Logger.push('info', `Register '${appName}' component: ${componentName}`, 'console')
    await Vue.component(componentName, component)
    return true
  }

  /**
   * registerAppModule
   * @category methods
   * @description Register an app store module
   * @param {string} appName - The app unique name
   * @param {string} moduleName - The store module name
   * @param {Object} moduleObject - The module object
   * @return {void}
   */
  async registerAppModule (appName, moduleName, moduleObject) {
    const safeModuleName = appName + '-' + moduleName
    Logger.push('info', `Register '${appName}' store module : ${safeModuleName}`, 'console')
    await Store.registerModule(safeModuleName, moduleObject)
    return true
  }

  /**
   * unregisterAppModule
   * @category methods
   * @description Unegister an app store module
   * @param {string} appName - The app unique name
   * @param {string} moduleName - The store module name
   * @return {void}
   */
  unregisterAppModule (appName, moduleName) {
    const safeModuleName = appName + '-' + moduleName
    Logger.push('info', `Unregister '${appName}' store module : ${safeModuleName}`, 'console')
    Store.unregisterModule(safeModuleName)
  }

  /**
   * generateAppModuleResourceMappers
   * @category methods
   * @description Generate app module resource mappers (state, getters, actions, mutations, etc...):
   * Create an object like:
   * ```javascript
   * // <identifier> is the name of the app resource
   * {
   *   state: {
   *     // camelized
   *     '<identifier>': [],
   *     '<identifier>Loading': false,
   *     '<identifier>Creating': false,
   *     '<identifier>Updating': false,
   *     '<identifier>Deleting': false
   *   },
   *   getters: {
   *     // camelized
   *     'find<identifier>'
   *   },
   *   actions: {
   *     // camelized
   *     'load<identifier>': Function,
   *     'create<identifier>': Function,
   *     'update<identifier>': Function,
   *     'delete<identifier>': Function
   *   },
   *   mutations: {
   *     // uppercase
   *     SET_<identifier>: Function,
   *     SET_<identifier>_LOADING: Function,
   *     SET_<identifier>_CREATING: Function,
   *     SET_<identifier>_UPDATING: Function,
   *     SET_<identifier>_DELETING: Function
   *   }
   * }
   * ```
   * @param {string} appName - App unique name
   * @param {string} resourceIdentifier - The resource identifier
   * @return {Object} Return a store module type object
   */
  generateAppModuleResourceMappers (appName, resourceIdentifier) {
    // Camelize resource identifier
    resourceIdentifier = camelize(resourceIdentifier)
    // 1st level property names
    const stateProp = camelize([resourceIdentifier, 'state'])
    const gettersProp = camelize([resourceIdentifier, 'getters'])
    const actionsProp = camelize([resourceIdentifier, 'actions'])
    const mutationsProp = camelize([resourceIdentifier, 'mutations'])
    // Other dynamic name
    const stateResourceName = resourceIdentifier
    const stateResourceLoadingName = camelize([resourceIdentifier, 'loading'])
    const stateResourceCreatingName = camelize([resourceIdentifier, 'creating'])
    const stateResourceUpdatingName = camelize([resourceIdentifier, 'updating'])
    const stateResourceDeletingName = camelize([resourceIdentifier, 'deleting'])
    const gettersFindResourceName = camelize(['find', resourceIdentifier])
    const actionsLoadResourceName = camelize(['load', resourceIdentifier])
    const actionsCreateResourceName = camelize(['create', resourceIdentifier])
    const actionsUpdateResourceName = camelize(['update', resourceIdentifier])
    const actionsDeleteResourceName = camelize(['delete', resourceIdentifier])
    const mutationsSetResourceName = uppercase(snakecase(['set', resourceIdentifier])).replace(' ', '_')
    const mutationsSetResourceLoadingName = uppercase(snakecase(['set', resourceIdentifier, 'loading'])).replace(' ', '_')
    const mutationsSetResourceCreatingName = uppercase(snakecase(['set', resourceIdentifier, 'creating'])).replace(' ', '_')
    const mutationsSetResourceUpdatingName = uppercase(snakecase(['set', resourceIdentifier, 'updating'])).replace(' ', '_')
    const mutationsSetResourceDeletingName = uppercase(snakecase(['set', resourceIdentifier, 'deleting'])).replace(' ', '_')
    // State
    const state = {
      [stateResourceName]: [], // Empty resource collection by default
      [stateResourceLoadingName]: false, // Indicator for all resource collection loading (load<ResourceIdentifier>() call)
      [stateResourceCreatingName]: false, // Indicator for a resource item creation (create<ResourceIdentifier>() call)
      [stateResourceUpdatingName]: false, // Indicator for a resource item updating (update<ResourceIdentifier>() call)
      [stateResourceDeletingName]: false // Indicator for a resource item deleting (delete<ResourceIdentifier>() call)
    }
    // Getters
    const getters = {
      // Retrieve an app resource item by property (values: Object)
      [gettersFindResourceName]: (state) => (values) => {
        // Find resource in [stateResourceName]
        return state[stateResourceName].reduce((array, item) => {
          // Initialize 'where' conditions
          const fields = {}
          // Foreach prop condition
          for (let prop in values) {
            // Assign to fields[prop] with equal checking
            fields[prop] = item[prop] === values[prop]
          }
          // If at least one 'false' field is returned
          let match = true // true by default
          for (let prop in fields) {
            if (!fields[prop]) {
              match = false
            }
          }
          if (match) {
            array.push(item)
          }
          return array
        }, [])
      }
    }
    // Actions
    const actions = {
      /**
       * load<ResourceIdentifier>() Main action to load all resource collection in state
       * Async indicators: state.<ResourceIdentifier>Loading
       * @return {Object|boolean} Return true if success, else Error
       */
      [actionsLoadResourceName]: async function ({ commit }) {
        const requestURL = `/apps/${appName}/resources/${resourceIdentifier}`
        /* eslint-disable operator-linebreak, quotes */
        Logger.push(
          'dev',
          `'${appName}' app module resource '${resourceIdentifier}' action '${actionsLoadResourceName}' called.\n`
          + `Request to: [GET] ${CONFIG.API.myStationAPI.baseURL}${requestURL}`,
          'console'
        )
        /* eslint-enable operator-linebreak, quotes */
        // Loading display
        const loadResponse = await window.__MYSTATION_VUE_INSTANCE__.wait('', async () => { // eslint-disable-line
          commit(mutationsSetResourceLoadingName, true) // Resource loading to true
          try {
            const response = await ServerAPI.request('GET', `${requestURL}`)
            commit(mutationsSetResourceName, response.data) // Commit received data
            commit(mutationsSetResourceLoadingName, false) // Resource loading to false
            return true
          } catch (error) {
            commit(mutationsSetResourceLoadingName, false) // Resource loading to false
            return error
          }
        }, { loading: [ 'logoLoading' ] })
        return loadResponse
      },
      /**
       * create<ResourceIdentifier>() Create resource
       * Async indicators: state.<ResourceIdentifier>Creating
       * @return {Object} Return resource object if success, else Error
       */
      [actionsCreateResourceName]: async function ({ commit }, resource) {
        const requestURL = `/apps/${appName}/resources/${resourceIdentifier}`
        /* eslint-disable operator-linebreak, quotes */
        Logger.push(
          'dev',
          `'${appName}' app module resource '${resourceIdentifier}' action '${actionsCreateResourceName}' called.\n`
          + `Request to: [POST] ${CONFIG.API.myStationAPI.baseURL}${requestURL}`,
          'console'
        )
        /* eslint-enable operator-linebreak, quotes */
        // Loading display
        const createResponse = await window.__MYSTATION_VUE_INSTANCE__.wait('', async () => { // eslint-disable-line
          commit(mutationsSetResourceCreatingName, true) // Resource creating to true
          try {
            const response = await ServerAPI.request('POST', `${requestURL}`, {
              data: {
                ...resource
              }
            })
            commit(mutationsSetResourceCreatingName, false) // Resource loading to false
            return response.data // Created
          } catch (error) {
            commit(mutationsSetResourceCreatingName, false) // Resource loading to false
            return error
          }
        }, { loading: [ 'logoLoading' ] })
        return createResponse
      },
      /**
       * update<ResourceIdentifier>() Update a resource
       * Async indicators: state.<ResourceIdentifier>Updating
       * @return {Object} Return resource object if success, else Error
       */
      [actionsUpdateResourceName]: async function ({ commit }, resource) {
        const requestURL = `/apps/${appName}/resources/${resourceIdentifier}`
        /* eslint-disable operator-linebreak, quotes */
        Logger.push(
          'dev',
          `'${appName}' app module resource '${resourceIdentifier}' action '${actionsUpdateResourceName}' called.\n`
          + `Request to: [PATCH] ${CONFIG.API.myStationAPI.baseURL}${requestURL}`,
          'console'
        )
        /* eslint-enable operator-linebreak, quotes */
        // Loading display
        const updateResponse = await window.__MYSTATION_VUE_INSTANCE__.wait('', async () => { // eslint-disable-line
          commit(mutationsSetResourceUpdatingName, true) // Resource updating to true
          try {
            const response = await ServerAPI.request('PATCH', `${requestURL}`, {
              data: {
                ...resource
              }
            })
            commit(mutationsSetResourceUpdatingName, false) // Resource updating to false
            return response.data
          } catch (error) {
            commit(mutationsSetResourceUpdatingName, false) // Resource updating to false
            return error
          }
        }, { loading: [ 'logoLoading' ] })
        return updateResponse
      },
      /**
       * delete<ResourceIdentifier>() Delete a resource
       * Async indicators: state.<ResourceIdentifier>Deleting
       * @return {Object} Return resource object if success, else Error
       */
      [actionsDeleteResourceName]: async function ({ commit }, resource) {
        const requestURL = `/apps/${appName}/resources/${resourceIdentifier}`
        /* eslint-disable operator-linebreak, quotes */
        Logger.push(
          'dev',
          `'${appName}' app module resource '${resourceIdentifier}' action '${actionsDeleteResourceName}' called.\n`
          + `Request to: [DELETE] ${CONFIG.API.myStationAPI.baseURL}${requestURL}`,
          'console'
        )
        /* eslint-enable operator-linebreak, quotes */
        // Loading display
        const deleteResponse = await window.__MYSTATION_VUE_INSTANCE__.wait('', async () => { // eslint-disable-line
          commit(mutationsSetResourceDeletingName, true) // Resource deleting to true
          try {
            const response = await ServerAPI.request('DELETE', `${requestURL}`, {
              data: {
                ...resource
              }
            })
            commit(mutationsSetResourceDeletingName, false) // Resource deleting to false
            return response.data
          } catch (error) {
            commit(mutationsSetResourceDeletingName, false) // Resource deleting to false
            return error
          }
        }, { loading: [ 'logoLoading' ] })
        return deleteResponse
      }
    }
    // Mutations
    const mutations = {
      /**
       * SET_<ResourceIdentifier>
       */
      [mutationsSetResourceName]: function (state, resourceCollection) {
        /* eslint-disable operator-linebreak, quotes */
        Logger.push(
          'dev',
          `'${appName}' app module resource '${resourceIdentifier}' mutation '${mutationsSetResourceName}' called.\n`,
          'console'
        )
        /* eslint-enable operator-linebreak, quotes */
        state[stateResourceName] = resourceCollection
      },
      /**
       * SET_<ResourceIdentifier>_LOADING
       */
      [mutationsSetResourceLoadingName]: function (state, value) {
        /* eslint-disable operator-linebreak, quotes */
        Logger.push(
          'dev',
          `'${appName}' app module resource '${resourceIdentifier}' mutation '${mutationsSetResourceLoadingName}' called.\n`,
          'console'
        )
        /* eslint-enable operator-linebreak, quotes */
        state[stateResourceLoadingName] = value
      },
      /**
       * SET_<ResourceIdentifier>_CREATING
       */
      [mutationsSetResourceCreatingName]: function (state, value) {
        /* eslint-disable operator-linebreak, quotes */
        Logger.push(
          'dev',
          `'${appName}' app module resource '${resourceIdentifier}' mutation '${mutationsSetResourceCreatingName}' called.\n`,
          'console'
        )
        /* eslint-enable operator-linebreak, quotes */
        state[stateResourceCreatingName] = value
      },
      /**
       * SET_<ResourceIdentifier>_UPDATING
       */
      [mutationsSetResourceUpdatingName]: function (state, value) {
        /* eslint-disable operator-linebreak, quotes */
        Logger.push(
          'dev',
          `'${appName}' app module resource '${resourceIdentifier}' mutation '${mutationsSetResourceUpdatingName}' called.\n`,
          'console'
        )
        /* eslint-enable operator-linebreak, quotes */
        state[stateResourceUpdatingName] = value
      },
      /**
       * SET_<ResourceIdentifier>_DELETING
       */
      [mutationsSetResourceDeletingName]: function (state, value) {
        /* eslint-disable operator-linebreak, quotes */
        Logger.push(
          'dev',
          `'${appName}' app module resource '${resourceIdentifier}' mutation '${mutationsSetResourceDeletingName}' called.\n`,
          'console'
        )
        /* eslint-enable operator-linebreak, quotes */
        state[stateResourceDeletingName] = value
      }
    }
    // Dev log
    /* eslint-disable operator-linebreak, quotes */
    Logger.push(
      'dev',
      `Generate '${appName}' app module resource mappers for '${resourceIdentifier}'.\n`
      + `Created: {
        ${stateProp}: {
          ${stateResourceName}: [],
          ${stateResourceLoadingName}: false,
          ${stateResourceCreatingName}: false,
          ${stateResourceUpdatingName}: false,
          ${stateResourceDeletingName}: false
        },
        ${gettersProp}: {
          ${gettersFindResourceName}()
        },
        ${actionsProp}: {
          ${actionsLoadResourceName}()
          ${actionsCreateResourceName}()
          ${actionsUpdateResourceName}()
          ${actionsDeleteResourceName}()
        },
        ${mutationsProp}: {
          ${mutationsSetResourceName}()
          ${mutationsSetResourceLoadingName}()
          ${mutationsSetResourceCreatingName}()
          ${mutationsSetResourceUpdatingName}()
          ${mutationsSetResourceDeletingName}()
        }
      }\n`
      + `Inject returned objects to store module.\n`,
      'console'
    )
    /* eslint-enable operator-linebreak, quotes */
    // Return object
    return {
      [stateProp]: state,
      [gettersProp]: getters,
      [actionsProp]: actions,
      [mutationsProp]: mutations
    }
  }

  /**
   * Install method for Vue
   * The service used as a plugin must absolutely overwrites this static method
   * @ignore
   * @param {Vue} _Vue - Vue
   * @param {Object} [options={}] - The options of the plugin
   */
  static install (_Vue, options = {}) {
    const Vue = _Vue

    const pluginName = 'AppManager'

    // Create a 'reactive' mixin
    Vue.mixin(ServiceAsPlugin.createMixin(true, pluginName))

    // Define the getter on the 'private' property
    Object.defineProperty(Vue.prototype, '$' + pluginName, {
      get () { return this['_' + pluginName] }
    })
  }
}

export default AppManager

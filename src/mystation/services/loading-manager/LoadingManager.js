/**
 * @vuepress
 * ---
 * title: LoadingManager class
 * headline: "MyStation LoadingManager class"
 * sidebarDepth: 0 # To disable auto sidebar links
 * prev: false # Disable prev link
 * next: false # Disable prev link
 * ---
 */

// Classes
import ServiceAsPlugin from '../ServiceAsPlugin.js'
import Loading from './Loading'

/**
 * LoadingManager class
 * @typicalname loadingManager
 * @extends ServiceAsPlugin
 * @classdesc
 * MyStation Loading Manager: LoadingManager class.
 * See also [ServiceAsPlugin]{@link ../ServiceAsPlugin}
 * ```javascript
 * // Import
 * import LoadingManager from '#mystation/services/loading-manager/LoadingManager'
 * ```
 */
class LoadingManager extends ServiceAsPlugin {
  /**
   * Create a new Loading manager
   * @param {Loading[]} [loadings = []] - The loadings to register
   */
  constructor (loadings = []) {
    let tmpLoadings = {}
    // Loop in receveid loadings array
    loadings.forEach((loading) => {
      // Check if loading is an instance of Loading class
      if (!(loading instanceof Loading)) {
        throw new TypeError('LoadingManager class: loadings must be instances of Loading class')
      }
      // Define loading
      tmpLoadings[loading.name] = loading
    })
    const initVMData = {
      loadings: tmpLoadings
    }
    // Call ServiceAsPlugin constructor
    super(true, initVMData)
  }

  /**
   * loadings
   * @category properties
   * @description loadings accessor
   * @return {Loading[]}
   */
  get loadings () {
    return this._vm.loadings
  }

  set loadings (loadings) {
    this._vm.loadings = loadings
    return this.loadings
  }

  /**
   * clear
   * @category methods
   * @description Clear : turn to false all loadings status in this.loadings
   * @return {void}
   */
  clear () {
    for (let loading in this.loadings) {
      this.loadings[loading].clear()
    }
  }

  /**
   * wait
   * @category methods
   * @description 'Wait' method to run loading during actions execution
   * @param {string} description - The description to fill
   * @param {function} [cb] - Callback to wait for. (can be async).
   * @param {Object} [options] - The options
   * @param {string[]} [options.loading] - The target(s) (array of loading names)
   * @return {boolean} Return true when all is executed and loading to false
   */
  async wait (description, cb = async () => { return true }, options = { loading: [ 'mainLoading' ] }) {
    let loading = {}
    options.loading.forEach((name) => {
      loading[name] = this.loadings[name].run(description)
    })
    const success = await cb()
    options.loading.forEach((name) => {
      this.loadings[name].stop(loading[name])
    })
    return success
  }

  /**
   * Install method for Vue
   * The service used as a plugin must absolutely overwrites this static method
   * @ignore
   * @param {Vue} _Vue - The Vue instance
   * @param {Object} [options={}] - The options of the plugin
   */
  static install (_Vue, options = {}) {
    const Vue = _Vue

    const pluginName = 'LoadingManager'

    // Create a 'reactive' mixin
    Vue.mixin(ServiceAsPlugin.createMixin(true, pluginName))

    // See ServiceAsPlugin 'createMixin' method documentation
    if (!Object.prototype.hasOwnProperty.call(Vue, '$' + pluginName)) {
      // Define the getter on the 'private' property
      Object.defineProperty(Vue.prototype, '$' + pluginName, {
        get () { return this['_' + pluginName] }
      })
      // Define an alias
      Object.defineProperty(Vue.prototype, '$loadings', {
        get () { return this['_' + pluginName].loadings }
      })
    }
  }
}

export default LoadingManager

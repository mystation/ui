/**
 * @vuepress
 * ---
 * title: Loading class
 * headline: "MyStation Loading class"
 * sidebarDepth: 0 # To disable auto sidebar links
 * prev: false # Disable prev link
 * next: false # Disable prev link
 * ---
 */

/**
 * Loading class
 * @typicalname loading
 * @classdesc
 * MyStation Loading Manager: Loading class
 * ```javascript
 * // Import
 * import Loading from '#mystation/services/loading-manager/Loading'
 * ```
 */
class Loading {
  /**
   * @private
   */
  _name; _defaultDescription; _queue;

  /**
   * Create a new Loading
   * @param {string} name - The loading name
   * @param {string} [defaultDescription = 'Loading'] - The default description
   */
  constructor (name, defaultDescription = 'Loading') {
    this.name = name
    this.defaultDescription = defaultDescription
    this._queue = []
  }

  /**
   * name
   * @category properties
   * @description Get the Loading name
   * @return {string}
   */
  get name () {
    return this._name
  }

  set name (value) {
    if (typeof value === 'string') {
      this._name = value
    } else {
      throw new TypeError('Loading class : the name must be String, ' + typeof value + ' received')
    }
  }

  /**
   * defaultDescription
   * @category properties
   * @description Get the Loading default description
   * @return {string}
   */
  get defaultDescription () {
    return this._defaultDescription
  }

  set defaultDescription (value) {
    if (typeof value === 'string' || value === null) {
      this._defaultDescription = value
    } else {
      throw new TypeError('Loading class : the default description must be String or null, ' + typeof value + ' received')
    }
  }

  /**
   * description
   * @category properties
   * @readonly
   * @description Get the last description in queue
   * @return {string}
   */
  get description () {
    if (this.status) {
      return this._queue[this._queue.length - 1].description
    } else {
      return this.defaultDescription
    }
  }

  /**
   * status
   * @category properties
   * @description Get the Loading status
   * @return {boolean}
   */
  get status () {
    return this._queue.length > 0
  }

  /**
   * run
   * @category methods
   * @description Run method: set description and status to true
   * @param {string} description - The description to fill
   * @param {string} [id] - ID: random string for unique key
   * @return {void}
   */
  run (description, id = Math.random().toString(36)) {
    this._queue.push({
      id: id,
      description: description
    })
    return id
  }

  /**
   * stop
   * @category methods
   * @description Stop method: remove item in queue by id
   * @param {string} id - ID: random string
   * @return {void}
   */
  stop (id) {
    let index
    this._queue.forEach((item, i) => {
      if (item.id === id) {
        index = i
      }
    })
    this._queue.splice(index, 1)
  }

  /**
   * clear
   * @category methods
   * @description Clear method: remove all item in queue
   * @return {void}
   */
  clear () {
    this._queue = []
  }
}

export default Loading

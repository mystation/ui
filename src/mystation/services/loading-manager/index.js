/**
 * @vuepress
 * ---
 * title: LoadingManager
 * headline: "MyStation service: LoadingManager"
 * prev: false # Disable prev link
 * next: false # Disable prev link
 * ---
 */

/**
 * MyStation service: LoadingManager
 * @module loading-manager
 * @description
 * MyStation service: LoadingManager
 * ```javascript
 * // Import
 * import LoadingManager from '#mystation/services/loading-manager'
 * ```
 */

import Vue from 'vue'
import Loading from './Loading'
import LoadingManager from './LoadingManager'

Vue.use(LoadingManager)

const mainLoading = new Loading('mainLoading')
const logoLoading = new Loading('logoLoading')

/**
 * LoadingManager
 * @alias module:loading-manager
 * @type {LoadingManager}
 * @constant
 * @description LoadingManager service
 * See also [LoadingManager class]{@link LoadingManager}
 * @vuepress_syntax_block LoadingManager
 * @vuepress_syntax_desc Access to LoadingManager instance
 * @vuepress_syntax_ctx {root}
 * this.$LoadingManager
 * this.$loadings // alias of 'this.$LoadingManager.loadings'
 * @vuepress_syntax_ctx {apps}
 * this.$LoadingManager
 * this.$loadings // alias of 'this.$LoadingManager.loadings'
 * __MYSTATION__.$LoadingManager
 * __MYSTATION__.$loadings // alias of '__MYSTATION__.$LoadingManager.loadings'
 * @vuepress_syntax_ctx {any}
 * window.__MYSTATION_VUE_INSTANCE__.$LoadingManager
 * window.__MYSTATION_VUE_INSTANCE__.$loadings // alias of 'window.__MYSTATION_VUE_INSTANCE__.$LoadingManager.loadings'
 */
export default new LoadingManager([
  mainLoading, // Main loading (first layer z-index max)
  logoLoading // 'logo' loading for MyStation top-left logo
])

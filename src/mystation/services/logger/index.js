/**
 * @vuepress
 * ---
 * title: Logger
 * headline: "MyStation service: Logger"
 * prev: false # Disable prev link
 * next: false # Disable prev link
 * ---
 */

/**
 * MyStation service: Logger
 * @module loading-manager
 * @description
 * MyStation service: Logger
 * ```javascript
 * // Import
 * import Logger from '#mystation/services/logger'
 * ```
 */

import Vue from 'vue'
// Classes
import Logger from './Logger'

Vue.use(Logger)

/**
 * Logger
 * @alias module:logger
 * @type {Logger}
 * @constant
 * @description Logger service
 * See also [Logger class]{@link Logger}
 * @vuepress_syntax_block Logger
 * @vuepress_syntax_desc Access to Logger instance
 * @vuepress_syntax_ctx {root}
 * this.$Logger
 * @vuepress_syntax_ctx {apps}
 * this.$Logger
 * __MYSTATION__.$Logger
 * @vuepress_syntax_ctx {any}
 * window.__MYSTATION_VUE_INSTANCE__.$Logger
 */
export default new Logger()

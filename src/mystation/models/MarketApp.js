/**
 * @vuepress
 * ---
 * title: MarketApp class
 * headline: "MyStation MarketApp class"
 * sidebarDepth: 0 # To disable auto sidebar links
 * prev: false # Disable prev link
 * next: false # Disable prev link
 * ---
 */

import minBy from 'lodash.minby'
import maxBy from 'lodash.maxby'

/**
 * MarketApp class
 * @typicalname marketApp
 * @classdesc
 * MyStation MarketApp model: MarketApp class.
 * ```javascript
 * // Import
 * import MarketApp from '#mystation/models/MarketApp'
 * ```
 */
class MarketApp {
  /**
   * @private
   */
  _name;
  _title;
  _icon;
  _color;
  _label;
  _author;
  _url;
  _description;
  _versions;

  /**
   * Create an App
   * @param {Object} data - Object that represents the app
   * @param {string} data.name - The unique app name
   * @param {string} data.title - The displayed name
   * @param {Buffer} data.icon - App icon (base 64)
   * @param {string} data.color - The color assigned to the app, in hexadecimal, with the '#' (ex: '#FAB3E7')
   * @param {Object} data.label - The translated app label
   * @param {Object} data.author - The app author
   * @param {string} data.url - The app url
   * @param {Object} data.description - The translated app description
   * @param {Object[]} data.versions - The app versions
   */
  constructor (data) {
    this._name = data.name
    this._title = data.title
    this._icon = data.icon
    this._color = data.color
    this._label = data.label
    this._author = data.author
    this._url = data.url
    this._description = data.description
    this._versions = data.versions
  }

  /**
   * name
   * @category properties
   * @readonly
   * @description App name accessor
   * @type {string}
   */
  get name () {
    return this._name
  }

  /**
   * title
   * @category properties
   * @readonly
   * @description App title accessor
   * @type {string}
   */
  get title () {
    return this._title
  }

  /**
   * icon
   * @category properties
   * @readonly
   * @description App icon accessor
   * @type {Buffer}
   */
  get icon () {
    return this._icon
  }

  /**
   * color
   * @category properties
   * @readonly
   * @description App color accessor
   * @type {string}
   */
  get color () {
    return this._color
  }

  /**
   * label
   * @category properties
   * @readonly
   * @description App label accessor
   * @type {Object}
   */
  get label () {
    return this._label
  }

  /**
   * author
   * @category properties
   * @readonly
   * @description App author accessor
   * @type {Object}
   */
  get author () {
    return this._author
  }

  /**
   * url
   * @category properties
   * @readonly
   * @description App url accessor
   * @type {Object}
   */
  get url () {
    return this._url
  }

  /**
   * description
   * @category properties
   * @readonly
   * @description App description accessor
   * @type {Object}
   */
  get description () {
    return this._description
  }

  /**
   * versions
   * @category properties
   * @readonly
   * @description App versions accessor
   * @type {Object[]}
   */
  get versions () {
    return this._versions
  }

  /**
   * lastVersion
   * @category methods
   * @description Return the last app version
   * @return {string}
   */
  lastVersion () {
    return maxBy(this.versions, function (v) { return v.version })
  }

  /**
   * firstVersion
   * @category methods
   * @description Return the first app version
   * @return {string}
   */
  firstVersion () {
    return minBy(this.versions, function (v) { return v.version })
  }

  /**
   * Default properties for a new empty 'MarketApp' object
   * @return {Object} Default properties object
   */
  static defaults () {
    return {
      name: '',
      title: '',
      icon: '',
      color: '',
      author: '',
      description: {},
      versions: []
    }
  }
}

export default MarketApp

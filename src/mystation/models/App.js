/**
 * @vuepress
 * ---
 * title: App class
 * headline: "MyStation App class"
 * sidebarDepth: 0 # To disable auto sidebar links
 * prev: false # Disable prev link
 * next: false # Disable prev link
 * ---
 */

/**
 * App class
 * @typicalname app
 * @classdesc
 * MyStation App model: App class.
 * ```javascript
 * // Import
 * import App from '#mystation/models/App'
 * ```
 */
class App {
  /**
   * @private
   */
  _name;
  _title;
  _version;
  _icon;
  _color;
  _appConfig;
  _pancakes;
  _resources;

  /**
   * Create an App
   * @param {Object} data - Object that represents the app
   * @param {string} data.name - The unique app name
   * @param {string} data.version - The app version (ex: '1.0.0')
   * @param {string} data.title - The displayed name
   * @param {Buffer} data.icon - App icon (base 64)
   * @param {string} data.color - The color assigned to the app, in hexadecimal, with the '#' (ex: '#FAB3E7')
   * @param {?Object} [data.app_config=null] - The app config for user configuration
   * @param {string[]} data.pancakes - The app pancakes component names
   * @param {Object[]} data.resources - The app resources
   */
  constructor (data) {
    this._name = data.name
    this._version = data.version
    this._title = data.title
    this._icon = data.icon
    this._color = data.color
    this._appConfig = data.app_config || null
    this._pancakes = data.pancakes
    this._resources = data.resources || []
  }

  /**
   * name
   * @category properties
   * @readonly
   * @description App name accessor
   * @type {string}
   */
  get name () {
    return this._name
  }

  /**
   * version
   * @category properties
   * @readonly
   * @description App version accessor
   * @type {string}
   */
  get version () {
    return this._version
  }

  /**
   * title
   * @category properties
   * @readonly
   * @description App title accessor
   * @type {string}
   */
  get title () {
    return this._title
  }

  /**
   * icon
   * @category properties
   * @readonly
   * @description App icon accessor
   * @type {Buffer}
   */
  get icon () {
    return this._icon
  }

  /**
   * color
   * @category properties
   * @readonly
   * @description App color accessor
   * @type {string}
   */
  get color () {
    return this._color
  }

  /**
   * appConfig
   * @category properties
   * @readonly
   * @description App appConfig accessor
   * @type {?Object}
   */
  get appConfig () {
    return this._appConfig
  }

  /**
   * pancakes
   * @category properties
   * @readonly
   * @description App pancakes accessor
   * @type {Object[]}
   */
  get pancakes () {
    return this._pancakes
  }

  /**
   * resources
   * @category properties
   * @readonly
   * @description App resources accessor
   * @type {Object[]}
   */
  get resources () {
    return this._resources
  }

  /**
   * Default properties for a new empty 'App' object
   * @return {Object} Default properties object
   */
  static defaults () {
    return {
      name: '',
      version: '',
      title: '',
      icon: '',
      color: '',
      appConfig: null,
      pancakes: [],
      resources: []
    }
  }
}

export default App

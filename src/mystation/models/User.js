/**
 * @vuepress
 * ---
 * title: User class
 * headline: "MyStation User class"
 * sidebarDepth: 0 # To disable auto sidebar links
 * prev: false # Disable prev link
 * next: false # Disable prev link
 * ---
 */

/**
 * User class
 * @typicalname user
 * @classdesc
 * MyStation User model: User class.
 * ```javascript
 * // Import
 * import User from '#mystation/models/User'
 * ```
 */
class User {
  /**
   * @private
   */
  _id;
  _firstname;
  _lastname;
  _username;
  _birthDate;
  _role;
  _createdAt;
  _updatedAt;
  _apps;
  _pancakes;
  _lastLogin;
  _authenticated;

  /**
   * Create a User
   * @param {Object} data - Object that represents the user
   * @param {string} data.id - The unique user id
   * @param {string} data.firstname - The user firstname
   * @param {string} data.lastname - The user lastname
   * @param {string} data.birth_date - The user birth date
   * @param {string} data.role - The user role
   * @param {string} data.created_at - When the user has been created
   * @param {string} data.updated_at - When the user has been updated
   * @param {?string} data.last_login - User last login date or null
   * @param {App[]} [data.apps] - The apps activated for the user
   * @param {boolean} [authenticated = false] - If the user is authenticated (default: false)
   */
  constructor (data, authenticated = false) {
    this._id = data.id
    this._firstname = data.firstname
    this._lastname = data.lastname
    this._username = data.username
    this._birthDate = new Date(data.birth_date)
    this._role = data.role
    this._createdAt = new Date(data.created_at)
    this._updatedAt = new Date(data.updated_at)
    this._apps = data.apps ? data.apps : []
    this._pancakes = data.pancakes ? data.pancakes : []
    this._lastLogin = data.last_login
    this._authenticated = authenticated
  }

  /**
   * id
   * @category properties
   * @readonly
   * @description User id accessor
   * @type {number}
   */
  get id () {
    return this._id
  }

  /**
   * firstname
   * @category properties
   * @readonly
   * @description User firstname accessor
   * @type {string}
   */
  get firstname () {
    return this._firstname
  }

  /**
   * lastname
   * @category properties
   * @readonly
   * @description User lastname accessor
   * @type {string}
   */
  get lastname () {
    return this._lastname
  }

  /**
   * username
   * @category properties
   * @readonly
   * @description User username accessor
   * @type {string}
   */
  get username () {
    return this._username
  }

  /**
   * birthDate
   * @category properties
   * @readonly
   * @description User birthDate accessor
   * @type {Date}
   */
  get birthDate () {
    return this._birthDate
  }

  /**
   * role
   * @category properties
   * @readonly
   * @description User role accessor
   * @type {string}
   */
  get role () {
    return this._role
  }

  /**
   * createdAt
   * @category properties
   * @readonly
   * @description User createdAt accessor
   * @type {Date}
   */
  get createdAt () {
    return this._createdAt
  }

  /**
   * updatedAt
   * @category properties
   * @readonly
   * @description User updatedAt accessor
   * @type {Date}
   */
  get updatedAt () {
    return this._updatedAt
  }

  /**
   * apps
   * @category properties
   * @readonly
   * @description User apps accessor
   * @type {App[]}
   */
  get apps () {
    return this._apps
  }

  /**
   * pancakes
   * @category properties
   * @readonly
   * @description User pancakes accessor
   * @type {Object[]}
   */
  get pancakes () {
    return this._pancakes
  }

  /**
   * lastLogin
   * @category properties
   * @readonly
   * @description User lastLogin accessor
   * @type {Object[]}
   */
  get lastLogin () {
    return this._lastLogin
  }

  /**
   * authenticated
   * @category properties
   * @description User authenticated accessor
   * @type {boolean}
   */
  get authenticated () {
    return this._authenticated
  }

  set authenticated (value) {
    this._authenticated = value
  }

  /**
   * fullName
   * @category methods
   * @description Returns concatenated firstname and lastname with a white space
   * @return {string}
   */
  fullName () {
    return this.firstname + ' ' + this.lastname
  }

  /**
   * getApp
   * @category methods
   * @description Returns the user app by unique name
   * @param {string} appName - The unique app name
   * @return {App|undefined}
   */
  getApp (appName) {
    return this.apps.find((app) => {
      return app.name === appName
    })
  }

  /**
   * Default properties for a new empty 'User' object
   * @return {Object} Default properties object
   */
  static defaults () {
    return {
      id: null,
      firstname: '',
      lastname: '',
      username: '',
      birthDate: null,
      role: 'user',
      createdAt: null,
      updatedAt: null,
      apps: [],
      pancakes: [],
      authenticated: false
    }
  }
}

export default User

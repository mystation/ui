/**
 * @vuepress
 * ---
 * title: Hook class
 * headline: "MyStation Hook class"
 * sidebarDepth: 0 # To disable auto sidebar links
 * prev: false # Disable prev link
 * next: false # Disable prev link
 * ---
 */

import kebabcase from 'lodash.kebabcase'

/**
 * Hook class
 * @typicalname hook
 * @classdesc
 * MyStation Hook class.
 * ```javascript
 * // Import
 * import Hook from '#mystation/Hook'
 * ```
 */
class Hook {
  /**
   * @private
   */
  _before;
  _after;
  _run;

  /**
   * Create a new Hook
   * @param {Object} options
   * @param {string} options.name - Name
   * @param {function} options.run - Main async 'run' Hook method
   * @param {function[]} [options.before] - Array of 'before' callback functions
   * @prama {function[]} [options.after] - Array of 'after' callback functions
   */
  constructor (options = {}) {
    this._name = options.name
    this._run = options.run
    this._before = options.before || []
    this._after = options.after || []
  }

  /**
   * name
   * @category properties
   * @readonly
   * @description Hook name accessor
   * @type {string}
   */
  get name () {
    return this._name
  }

  /**
   * run
   * @category properties
   * @readonly
   * @description Hook run accessor
   * Execute [before()] -> this._run() -> [after()]
   * @type {function}
   */
  get run () {
    // Here, 'this' is the Hook instance
    const beforeFcts = this._before
    const afterFcts = this._after
    const run = this._run
    return async function () {
      // Here, 'this' is the MyStation Vue Instance
      // Each 'before' action
      for (const b of beforeFcts) {
        // Call before
        await b.call(this) // 'transfer' this to child context
      }
      // Run
      await run.call(this) // 'transfer' this to child context
      // Each 'after' action
      for (const a of afterFcts) {
        // Call after
        await a.call(this) // 'transfer' this to child context
      }
    }
  }

  set run (arg) {
    throw new Error('MyStation [ERROR]: "Hook" class -> Cannot set readonly "run" attribute')
  }

  /**
   * before
   * @category properties
   * @readonly
   * @description Hook before accessor
   * @type {Object}
   */
  get before () {
    // Return object with 'append' added method
    return {
      get () { return this._before },
      append: (fct) => {
        /* eslint-disable-next-line */
        console.log('Add instructions to execute before ' + this.name + ' hook (' + this.triggerName + ')')
        this._before.push(fct) // Push to array. See mutator
      }
    }
  }

  set before (arg) {
    throw new Error('MyStation [ERROR]: "Hook" class -> readonly "before" attribute. Use append() method instead.')
  }

  /**
   * after
   * @category properties
   * @readonly
   * @description Hook after accessor
   * @type {Object}
   */
  get after () {
    // Return object with 'append' added method
    return {
      get () { return this._after },
      append: (fct) => {
        /* eslint-disable-next-line */
        console.log('Add instructions to execute after ' + this.name + ' hook (' + this.triggerName + ')')
        this._after.push(fct) // Push to array. See mutator
      }
    }
  }

  set after (arg) {
    throw new Error('MyStation [ERROR]: "Hook" class -> readonly "after" attribute. Use append() method instead.')
  }

  /**
   * triggerName
   * @category properties
   * @readonly
   * @description Hook triggerName accessor (EventBus event name)
   * @type {string}
   */
  get triggerName () {
    // Return Ex: name-formatted-hook
    return kebabcase(this.name + '-hook')
  }
}

export default Hook

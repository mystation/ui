/**
 * @vuepress
 * ---
 * title: MyStation API
 * headline: "MyStation API"
 * sidebarDepth: 0 # To disable auto sidebar links
 * prev: false # Disable prev link
 * next: false # Disable prev link
 * ---
 */

/**
 * MyStation API
 * @module mystation
 * @description
 * MyStation API
 * ```javascript
 * // Import
 * import MyStation from '#mystation'
 * ```
 */

// Classes
import Hook from './Hook.js'
import APIClass from '#mystation/services/api-manager/API'
import APIManagerClass from '#mystation/services/api-manager/APIManager'
import APIOptionsClass from '#mystation/services/api-manager/APIOptions'
import AppManagerClass from '#mystation/services/app-manager/AppManager'
import ErrorManagerClass from '#mystation/services/error-manager/ErrorManager'
import HelpersClass from '#mystation/services/helpers/Helpers'
import LoadingClass from '#mystation/services/loading-manager/Loading'
import LoadingManagerClass from '#mystation/services/loading-manager/LoadingManager'
import LoggerClass from '#mystation/services/logger/Logger'
import ServiceAsPluginClass from '#mystation/services/ServiceAsPlugin'

// Components
import AppContainer from '@/components/mainframe/AppContainer'
import BaseAppComponent from '@/components/mainframe/BaseAppComponent'

// Internationalization
import I18n from '@/i18n'

// Store
import Store from '@/store'

// Services
import APIManager from '#mystation/services/api-manager'
import AppManager from '#mystation/services/app-manager'
import ErrorManager from '#mystation/services/error-manager'
import EventBus from '#mystation/services/event-bus'
import Helpers from '#mystation/services/helpers'
import LoadingManager from '#mystation/services/loading-manager'
import Logger from '#mystation/services/logger'

// Modules
import auth from '#mystation/modules/auth'
import emit from '#mystation/modules/emit'
import generate from '#mystation/modules/generate'
import hasInstalledApp from '#mystation/modules/hasInstalledApp'
import initializeApp from '#mystation/modules/initializeApp'
import isAppInitialized from '#mystation/modules/isAppInitialized'
import log from '#mystation/modules/log'
import network from '#mystation/modules/network'
import on from '#mystation/modules/on'
import registerApp from '#mystation/modules/registerApp'
import ui from '#mystation/modules/ui'
import updateApp from '#mystation/modules/updateApp'
import wait from '#mystation/modules/wait'

// ---- END IMPORTS

/**
 * init
 * @ignore
 * @description MyStation 'init' function
 * Initialize all necessary...
 * @return {void}
 */
async function init () {
  // Here, 'this' is MyStation Vue instance
  // Assign modules
  // 'proxify' function: to transform MyStation module object to 'proxied' object
  // for which all 'function' type properties are 'encapsulated' by arrow function
  // that call the function itself with the 'this' context of the MyStation root instance
  // So, if a MyStation module is an object (and not simply a function), all its child property
  // that is a function will access to 'this' root context
  const proxify = (target) => {
    let tmp = new Proxy({}, {
      // Overwrite Object type getter to 'observe'
      get: (obj, prop) => {
        if (prop in obj) {
          // If the module object property is a function, encapsulate with root 'this' context
          if (typeof obj[prop] === 'function') {
            return (...args) => { return obj[prop].call(this, ...args) }
          } else {
            return obj[prop]
          }
        }
        // Else return undefined
        return undefined
      },
      // Overwrite Object type setter
      set: function (obj, prop, value) {
        if (typeof value === 'object') {
          obj[prop] = proxify(value)
        } else {
          obj[prop] = value
        }
        return true
      }
    })
    // Loop on received object properties to start recursivity
    for (let property in target) {
      tmp[property] = target[property]
    }
    return tmp
  }
  // Loop on each module and 'proxify' if it's a object
  for (const moduleName in MyStation.modules) {
    let module
    if (typeof MyStation.modules[moduleName] === 'object') {
      module = proxify(MyStation.modules[moduleName])
    } else {
      // Else, assign simply to MyStation root instance
      module = MyStation.modules[moduleName]
    }
    Object.defineProperty(this, moduleName, { get () { return module } })
  }
  // Loop on hooks from main object
  for (const hookName in main) {
    // Event name
    const triggerName = main[hookName].triggerName
    // Declare hook trigger in the global event bus
    this.on(triggerName, async () => {
      // Here, 'this' is MyStation Vue instance
      // Call the run method
      await main[hookName].run.call(this) // transfer 'this' using call()
    })
  }
  // Set the default description for the main loading
  this.$loadings.mainLoading.defaultDescription = this.$h.capitalize(I18n.t('global.loading'))
  // OK
  return true
}

/**
 * main
 * @ignore
 * @description MyStation 'main()' flow splitted by hooks
 * @type {Object}
 */
const main = {
  /**
   * BOOT
   * @type {Hook}
   * @description First step
   */
  BOOT: new Hook({
    // Name
    name: 'BOOT',
    // Run method
    run: async function () {
      // Here, 'this' is the MyStation Vue Instance
      // If local development mode enabled
      if (process.env.NODE_ENV === 'development') {
        this.log('dev', 'Development mode enabled', 'console')
      }
      this.log('info', 'Booting', 'console')
      // Loading display
      const authResponse = await this.wait(this.$h.capitalize(I18n.t('global.loadingDescriptions.authentication')), async () => {
        this.log('info', 'Check authentication...', 'console')
        const response = await this.auth.check()
        if (!response) {
          this.log('info', 'Unauthenticated. Please log in', 'console')
        }
        return response
      }, { loading: [ 'mainLoading', 'logoLoading' ] })
      // If user authenticated
      if (authResponse) {
        // Emit event on root event bus to trigger USER_LOGGED hook
        this.emit('user-logged-hook')
      }
      // Else STOP
      return true
    }
  }),
  /**
   * USER_LOGGED
   * @type {Hook}
   * @description Called when user is logged
   */
  USER_LOGGED: new Hook({
    // Name
    name: 'USER_LOGGED',
    // Run method
    run: async function () {
      // Here, 'this' is the MyStation Vue Instance
      this.log('info', 'User logged', 'console')
      // Load last version from web API
      this.$store.dispatch('getLastVersion')
      let systemData
      // System data loading
      await this.wait(this.$h.capitalize(I18n.t('global.loadingDescriptions.systemDataLoading')), async () => {
        this.log('info', 'Load apps data...', 'console')
        systemData = await this.$store.dispatch('loadSystemData') // Return true or error
      }, { loading: [ 'mainLoading', 'logoLoading' ] })
      if (systemData instanceof Error) {
        // Authentication token error
        this.$ErrorManager.throw('e0102', systemData) // systemData is an error
      }
      let usersData
      // Users data loading
      await this.wait(this.$h.capitalize(I18n.t('global.loadingDescriptions.usersDataLoading')), async () => {
        this.log('info', 'Load users data...', 'console')
        usersData = await this.$store.dispatch('Users/loadUsersData') // Return true or error
      }, { loading: [ 'mainLoading', 'logoLoading' ] })
      if (systemData instanceof Error) {
        // Authentication token error
        this.$ErrorManager.throw('e0102', usersData) // usersData is an error
      }
      let loadApps
      // Apps loading
      await this.wait(this.$h.capitalize(I18n.t('global.loadingDescriptions.appsLoading')), async () => {
        this.log('info', 'Load apps data...', 'console')
        loadApps = await this.$store.dispatch('Apps/loadApps') // Return true or error
      }, { loading: [ 'mainLoading', 'logoLoading' ] })
      if (loadApps === true) {
        // Emit event on root event bus to trigger APPS_LOADED hook
        this.emit('apps-data-loaded-hook')
      } else {
        // Authentication token error
        this.$ErrorManager.throw('e0102', loadApps) // loadApps is an error
      }
      // STOP
      return true
    }
  }),
  /**
   * APPS_DATA_LOADED
   * @type {Hook}
   * Called when apps data is loaded (store: Apps/loadApps)
   */
  APPS_DATA_LOADED: new Hook({
    // Name
    name: 'APPS_DATA_LOADED',
    // Run method
    run: async function () {
      this.log('info', 'Apps data loaded', 'console')
      // Loading display
      await this.wait(this.$h.capitalize(I18n.t('global.loadingDescriptions.appsBundlesLoading')), async () => {
        // Synchronize App Manager with store apps data
        this.log('info', 'Synchronizing App Manager', 'console')
        AppManager.sync()
        // Load app sources
        await AppManager.loadSources()
      }, { loading: [ 'mainLoading', 'logoLoading' ] })
      // Display Dashboard
      this.ui.display('Dashboard')
    }
  }),
  /**
   * APPS_BUNDLES_LOADED
   * @type {Hook}
   * Called when apps bundles are loaded
   */
  APPS_BUNDLES_LOADED: new Hook({
    // Name
    name: 'APPS_BUNDLES_LOADED',
    // Run method
    run: async function () {
      this.log('info', 'Apps bundles loaded', 'console')
    }
  })
}

/**
 * boot
 * @ignore
 * @description Must be called by MyStation Vue instance in mounted() method
 * @return {void}
 */
async function boot () {
  // Inititialize first
  await init.call(this) // pass 'this' to child context with call() method
  // Then boot
  // Emit event to trigger BOOT hook
  this.emit('boot-hook')
}

/**
 * MyStation
 * @alias module:mystation
 * @type {Object}
 */
const MyStation = {
  /**
   * class
   * @alias module:mystation.class
   * @type {Object}
   */
  class: {
    /**
     * API
     * @type {function}
     * @description API constructor
     */
    API: APIClass,
    /**
     * APIManager
     * @type {function}
     * @description APIManager constructor
     */
    APIManager: APIManagerClass,
    /**
     * APIOptions
     * @type {function}
     * @description APIOptions constructor
     */
    APIOptions: APIOptionsClass,
    /**
     * AppManager
     * @type {function}
     * @description AppManager constructor
     */
    AppManager: AppManagerClass,
    /**
     * ErrorManager
     * @type {function}
     * @description ErrorManager constructor
     */
    ErrorManager: ErrorManagerClass,
    /**
     * Helpers
     * @type {function}
     * @description Helpers constructor
     */
    Helpers: HelpersClass,
    /**
     * Hook
     * @type {function}
     * @description Hook constructor
     */
    Hook: Hook,
    /**
     * Loading
     * @type {function}
     * @description Loading constructor
     */
    Loading: LoadingClass,
    /**
     * LoadingManager
     * @type {function}
     * @description LoadingManager constructor
     */
    LoadingManager: LoadingManagerClass,
    /**
     * Logger
     * @type {function}
     * @description Logger constructor
     */
    Logger: LoggerClass,
    /**
     * ServiceAsPlugin
     * @type {function}
     * @description ServiceAsPlugin constructor
     */
    ServiceAsPlugin: ServiceAsPluginClass
  },
  /**
   * components
   * @alias module:mystation.components
   * @type {Object}
   */
  components: {
    /**
     * AppContainer
     * @type {VueComponent}
     * @description AppContainer component
     */
    AppContainer: AppContainer,
    /**
     * BaseAppComponent
     * @type {VueComponent}
     * @description BaseAppComponent component
     */
    BaseAppComponent: BaseAppComponent
  },
  /**
   * system
   * @alias module:mystation.system
   * @type {Object}
   */
  system: {
    /**
     * I18n
     * @type {I18n}
     * @description I18n (see [I18n]{@link ../i18n})
     */
    I18n: I18n,
    /**
     * Store
     * @type {Vuex.Store}
     * @description Store (see [Store]{@link ../store})
     */
    Store: Store,
    /**
     * boot
     * @type {function}
     * @description Boot method
     */
    boot: boot,
    /**
     * errorHandler
     * @type {function}
     * @description Error handler
     */
    errorHandler: function (error, vm, info) {
      /* TO DO: error handler doesn't catch in app store module... */
      ErrorManager.throw('e0000', error)
      Logger.push('error', error.message, 'console')
      console.error(error) // eslint-disable-line
      return false
    }
  },
  /**
   * modules
   * @type {Object}
   */
  modules: {
    /**
     * auth
     * @type {Object}
     * @description auth module (see [auth]{@link modules/auth})
     */
    auth, // Object
    /**
     * emit
     * @type {function}
     * @description emit module (see [emit]{@link modules/emit})
     */
    emit,
    /**
     * generate
     * @type {Object}
     * @description generate module (see [generate]{@link modules/generate})
     */
    generate, // Object
    /**
     * hasInstalledApp
     * @type {function}
     * @description hasInstalledApp module (see [hasInstalledApp]{@link modules/hasInstalledApp})
     */
    hasInstalledApp,
    /**
     * initializeApp
     * @type {function}
     * @description initializeApp module (see [initializeApp]{@link modules/initializeApp})
     */
    initializeApp,
    /**
     * isAppInitialized
     * @type {function}
     * @description isAppInitialized module (see [isAppInitialized]{@link modules/isAppInitialized})
     */
    isAppInitialized,
    /**
     * log
     * @type {function}
     * @description log module (see [log]{@link modules/log})
     */
    log,
    /**
     * network
     * @type {Object}
     * @description network module (see [network]{@link modules/network})
     */
    network, // Object
    /**
     * on
     * @type {function}
     * @description on module (see [on]{@link modules/on})
     */
    on,
    /**
     * registerApp
     * @type {function}
     * @description registerApp module (see [registerApp]{@link modules/registerApp})
     */
    registerApp,
    /**
     * ui
     * @type {Object}
     * @description ui module (see [ui]{@link modules/ui})
     */
    ui, // Object
    /**
     * updateApp
     * @type {Object}
     * @description updateApp module (see [updateApp]{@link modules/updateApp})
     */
    updateApp,
    /**
     * wait
     * @type {function}
     * @description wait module (see [wait]{@link modules/wait})
     */
    wait
  },
  /**
   * services
   * @type {Object}
   */
  services: {
    /**
     * APIManager
     * @type {APIManager}
     * @description APIManager service (see [APIManager]{@link services/APIManager})
     */
    APIManager: APIManager,
    /**
     * AppManager
     * @type {AppManager}
     * @description AppManager service (see [AppManager]{@link services/AppManager})
     */
    AppManager: AppManager,
    /**
     * ErrorManager
     * @type {ErrorManager}
     * @description ErrorManager service (see [ErrorManager]{@link services/ErrorManager})
     */
    ErrorManager: ErrorManager,
    /**
     * EventBus
     * @type {Vue}
     * @description EventBus service (see [EventBus]{@link services/EventBus})
     */
    EventBus: EventBus,
    /**
     * Helpers
     * @type {Helpers}
     * @description Helpers service (see [Helpers]{@link services/Helpers})
     */
    Helpers: Helpers,
    /**
     * LoadingManager
     * @type {LoadingManager}
     * @description LoadingManager service (see [LoadingManager]{@link services/LoadingManager})
     */
    LoadingManager: LoadingManager,
    /**
     * Logger
     * @type {Logger}
     * @description Logger service (see [Logger]{@link services/Logger})
     */
    Logger: Logger
  }
}

export default MyStation

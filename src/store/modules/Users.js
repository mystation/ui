/**
 * @vuepress
 * ---
 * title: Users
 * headline: "MyStation store: Users module"
 * sidebarDepth: 0 # To disable auto sidebar links
 * prev: false # Disable prev link
 * next: false # Disable prev link
 * ---
 */

/**
 * MyStation store module for users
 * @module store/modules/Users
 * @description
 * MyStation store module: Vuex Store module
 * ```javascript
 * // Import
 * import Users from '@/store/modules/Users'
 * ```
 */

// Configuration
import CONFIG from '@/config/config.js'
// Models
import User from '#mystation/models/User'
import App from '#mystation/models/App'
// APIManager
import APIManager from '#mystation/services/api-manager/index'

// Define MyStation local API
const ServerAPI = APIManager.use('myStationAPI')

/**
 * Users
 * @alias module:store/modules/Users
 * @type {Object}
 */
const Users = {
  /**
   * @private
   */
  namespaced: true,
  /**
   * state
   * @type {Object}
   * @readonly
   * @description Store module state
   * @example
   * // Access to the module state
   * Store.state.Users
   */
  state: {
    /**
     * users
     * @description Users data received from server.
     * Typically, the local users
     * @type {User[]}
     * @default []
     * @example
     * // Access to property
     * Store.state.Users.users
     */
    users: [],
    /**
     * usersLoading
     * @description Indicates if users are loading from API
     * @type {boolean}
     * @default false
     */
    usersLoading: false,
    /**
     * currentUser
     * @description Current / Authenticated User
     * @type {User}
     * @default new User()
     */
    currentUser: new User(User.defaults()),
    /**
     * currentUserLoading
     * @description Indicates if current user is loading from API
     * @type {boolean}
     * @default false
     */
    currentUserLoading: false
  },
  /**
   * getters
   * @type {Object}
   * @readonly
   * @description Store module getters
   * @example
   * // Access to the module getters, where <name> is the getter name
   * Store.getters['Users/<name>']
   */
  getters: {
    /**
     * getUser
     * @description Retrieve a user by id
     * @type {function}
     * @param {number} id - User id
     * @return {(User|undefined)}
     */
    getUser: (state) => (id) => {
      // Find user in state.users
      return state.users.find(user => user.id === id)
    }
  },
  /**
   * actions
   * @type {Object}
   * @protected
   * @description Store module actions
   * @example
   * // Access to the module action, where <name> is the action name
   * Store.dispatch('Users/<name>')
   */
  actions: {
    /**
     * login
     * @description Request `[POST] /api/oauth/token` with credentials data (See [API Server documentation](/guide/system/api-server/)).
     * If server side success, save tokens in LocalStorage and dispatch 'checkAuthenticated'
     * @param {Object} credentials - The user credentials
     * @param {string} credentials.username - Username
     * @param {string} credentials.password - Password
     * @return {(boolean|Error)} Return value of 'checkAuthenticated'
     */
    login: async function ({ commit, dispatch }, credentials) {
      // Set currentUserLoading to true
      commit('SET_CURRENT_USER_LOADING', true)
      let authResponse = null
      let accessToken = null
      let refreshToken = null
      // Request for auth
      try {
        authResponse = await ServerAPI.request('POST', '/oauth/token', {
          data: {
            grant_type: 'password',
            client_id: 1,
            client_secret: 'My5t4ti0n',
            ...credentials
          }
        })
        accessToken = authResponse.data.access_token
        refreshToken = authResponse.data.refresh_token
        // Save tokens in localStorage
        localStorage.setItem(CONFIG.AUTH.ACCESS_TOKEN_NAME, accessToken)
        localStorage.setItem(CONFIG.AUTH.REFRESH_TOKEN_NAME, refreshToken)
      } catch (error) {
        // Set currentUserLoading to false
        commit('SET_CURRENT_USER_LOADING', false)
        return error
      }
      // Get user
      return dispatch('checkAuthenticated')
    },
    /**
     * logout
     * @description Remove tokens from the local storage and reset current user
     * @return {void}
     */
    logout: async function ({ commit }) {
      try {
        // Request for logout
        await ServerAPI.request('POST', '/me/logout')
      } catch (error) {
        // TO DO ?...
      }
      // Remove access token
      localStorage.removeItem(CONFIG.AUTH.ACCESS_TOKEN_NAME)
      localStorage.removeItem(CONFIG.AUTH.REFRESH_TOKEN_NAME)
      // Reset currentUser
      commit('RESET_CURRENT_USER')
    },
    /**
     * checkAuthenticated
     * @description Check if user is authenticated.
     * First, check if token is set in the local storage,
     * set authorization header and request for user data from server.
     * Else, call 'logout' and return false.
     * @return {(boolean|Error)} False or return value of 'loadCurrentUserData'
     */
    checkAuthenticated: async function ({ commit, dispatch }) {
      // Set currentUserLoading to true
      commit('SET_CURRENT_USER_LOADING', true)
      // Retrieve token in localStorage
      const accessToken = localStorage.getItem(CONFIG.AUTH.ACCESS_TOKEN_NAME)
      // const refreshToken = localStorage.getItem('ms_refresh_token')
      // If no access token
      if (!accessToken) {
        // Set currentUserLoading to false
        commit('SET_CURRENT_USER_LOADING', false)
        return false
      }
      // Set token to headers for each future requests
      dispatch('setAuthorizationHeader', accessToken)
      // Get User data
      return dispatch('loadCurrentUserData')
    },
    /**
     * loadCurrentUserData
     * @description Load current user data from API server.
     * Request `[GET] /api/me` (See [API Server documentation](/guide/system/api-server/)).
     * @return {(boolean|Error)} If success: true, else Error
     */
    loadCurrentUserData: async function ({ commit, dispatch }) {
      // Set currentUserLoading to true
      commit('SET_CURRENT_USER_LOADING', true)
      // Get authenticated user data
      try {
        const userResponse = await ServerAPI.request('GET', '/me')
        // Set user in state
        commit('SET_CURRENT_USER', userResponse.data)
      } catch (error) {
        // Authentication token error
        // Remove token and reset user
        dispatch('logout')
        // Set currentUserLoading to false
        commit('SET_CURRENT_USER_LOADING', false)
        // Check server response for admin password change
        if (error.response.status === 403 && error.response.data.forbidden && error.response.data.forbidden.ADMIN_PSSWD_CHANGED === false) {
          commit('SET_MUST_ADMIN_PASSWORD_BE_CHANGED', true, { root: true })
          return false
        }
        return error
      }
      // Set currentUserLoading to false
      commit('SET_CURRENT_USER_LOADING', false)
      // Return true
      return true
    },
    /**
     * loadUsersData
     * @description Load users data from API server.
     * Request `[GET] /api/users` (See [API Server documentation](/guide/system/api-server/)).
     * @return {(boolean|Error)} If success: true, else Error
     */
    loadUsersData: async function ({ commit }) {
      // Set usersLoading to true
      commit('SET_USERS_LOADING', true)
      // Get users data
      try {
        const usersResponse = await ServerAPI.request('GET', '/users')
        // Set user in state
        commit('SET_USERS', usersResponse.data)
      } catch (error) {
        commit('SET_USERS_LOADING', false)
        return error
      }
      // Set usersLoading to false
      commit('SET_USERS_LOADING', false)
      // Return true
      return true
    },
    /**
     * setAuthorizationHeader
     * @description Set the authorization token header for the API instance (ServerAPI).
     * @param {string} token - The access token
     * @return {void}
     */
    setAuthorizationHeader: function ({ commit }, token) {
      // Set authorization token
      ServerAPI.driver.defaults.headers.common.Authorization = 'Bearer ' + token
    },
    /**
     * resetAuthorizationHeader
     * @description Delete the authorization token header for the API instance (ServerAPI).
     * @return {void}
     */
    resetAuthorizationHeader ({ commit }) {
      // Delete header
      delete ServerAPI.driver.defaults.headers.common.Authorization
    },
    /**
     * initializeApp
     * @description App initialization method.
     * Request `[POST] /api/apps/<appName>/init` (See [API Server documentation](/guide/system/api-server/))
     * to initialize an app for the current authenticated user.
     * Then, load current user data from server: dispatch 'loadCurrentUserData'.
     * @param {string} appName - The unique app name
     * @return {(boolean|Error)} Error or return value of 'loadCurrentUserData'
     */
    initializeApp: async function ({ commit, dispatch }, appName) {
      try {
        await ServerAPI.request('POST', `/apps/${appName}/init`)
      } catch (error) {
        return error
      }
      return dispatch('loadCurrentUserData')
    },
    /**
     * resetUserPassword
     * @description Reset user password method.
     * Request `[POST] /api/users/<userId>/resetPassword` (See [API Server documentation](/guide/system/api-server/))
     * to reset a user password.
     * @param {Object} payload - Method payload
     * @param {number} payload.userId - User id
     * @param {string} payload.newPassword - New password
     * @return {(boolean|Error)} Error or return true
     */
    resetUserPassword: async function ({ commit }, payload) {
      try {
        await ServerAPI.request('POST', `/users/${payload.userId}/resetPassword`, {
          data: {
            newPassword: payload.newPassword
          }
        })
      } catch (error) {
        return error
      }
      return true
    },
    /**
     * createUser
     * @description Create user method.
     * Request `[POST] /api/users/create` (See [API Server documentation](/guide/system/api-server/))
     * to create a new user.
     * @param {Object} payload - Method payload
     * @param {string} payload.username - User username
     * @param {string} payload.firstname - User firstname
     * @param {string} payload.lastname - User lastname
     * @param {string} payload.password - User password
     * @return {(boolean|Error)} Error or return true
     */
    createUser: async function ({ dispatch }, payload) {
      try {
        await ServerAPI.request('POST', `/users/create`, {
          data: payload
        })
      } catch (error) {
        return error
      }
      await dispatch('loadUsersData')
      return true
    },
    /**
     * deleteUser
     * @description Delete user method.
     * Request `[POST] /api/users/<userId>/delete` (See [API Server documentation](/guide/system/api-server/))
     * to delete a user.
     * @param {Object} userId - user id
     * @return {(boolean|Error)} Error or return true
     */
    deleteUser: async function ({ dispatch }, userId) {
      try {
        await ServerAPI.request('POST', `/users/${userId}/delete`)
      } catch (error) {
        return error
      }
      await dispatch('loadUsersData')
      return true
    }
  },
  /**
   * mutations
   * @type {Object}
   * @protected
   * @description Store module mutations
   * @example
   * // Dispatch a module mutation, where <mutation_name> is the mutation name
   * Store.commit('Users/<mutation_name>', [payload])
   */
  mutations: {
    /**
     * SET_USERS
     * @description Mutate state.users
     * @param {Object[]} users - Data received from server
     * @return {void}
     */
    SET_USERS (state, users) {
      state.users = users.map((user) => {
        return new User(user)
      })
    },
    /**
     * SET_USERS_LOADING
     * @description Mutate state.usersLoading
     * @param {boolean} value - True or false
     * @return {void}
     */
    SET_USERS_LOADING (state, value) {
      state.usersLoading = value
    },
    /**
     * SET_CURRENT_USER
     * @description Mutate state.currentUser
     * @param {Object} user - Data received from server
     * @return {void}
     */
    SET_CURRENT_USER (state, user) {
      user.apps = user.apps.map((app) => {
        return new App(app)
      })
      state.currentUser = new User(user)
      state.currentUser.authenticated = true
    },
    /**
     * RESET_CURRENT_USER
     * @description Mutate state.currentUser
     * @return {void}
     */
    RESET_CURRENT_USER (state) {
      state.currentUser = new User(User.defaults())
    },
    /**
     * SET_CURRENT_USER_LOADING
     * @description Mutate state.currentUserLoading
     * @param {boolean} value - True or false
     * @return {void}
     */
    SET_CURRENT_USER_LOADING (state, value) {
      state.currentUserLoading = value
    }
  }
}

export default Users

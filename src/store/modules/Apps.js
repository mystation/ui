/**
 * @vuepress
 * ---
 * title: Apps
 * headline: "MyStation store: Apps module"
 * sidebarDepth: 0 # To disable auto sidebar links
 * prev: false # Disable prev link
 * next: false # Disable prev link
 * ---
 */

/**
 * MyStation store module for apps
 * @module store/modules/Apps
 * @description
 * MyStation store module: Vuex Store module
 * ```javascript
 * // Import
 * import Apps from '@/store/modules/Apps'
 * ```
 */

// Models
import App from '#mystation/models/App'
import MarketApp from '#mystation/models/MarketApp'
// APIManager
import APIManager from '#mystation/services/api-manager/index'

// Define MyStation local API
const ServerAPI = APIManager.use('myStationAPI')

/**
 * Apps
 * @alias module:store/modules/Apps
 * @type {Object}
 */
const Apps = {
  /**
   * @private
   */
  namespaced: true,
  /**
   * state
   * @type {Object}
   * @readonly
   * @description Store module state
   * @example
   * // Access to the module state
   * Store.state.Apps
   */
  state: {
    /**
     * apps
     * @description Apps data received from server.
     * Typically, the local installed apps
     * @type {App[]}
     * @default []
     * @example
     * // Access to property
     * Store.state.Apps.apps
     */
    apps: [],
    /**
     * appsLoading
     * @description Indicates if apps are loading from API
     * @type {boolean}
     * @default false
     */
    appsLoading: false,
    /**
     * marketApps
     * @description Market Apps data received from MyStation official API server.
     * @type {MarketApp[]}
     * @default []
     */
    marketApps: [],
    /**
     * marketAppsLoading
     * @description Indicates if market apps are loading from API
     * @type {boolean}
     * @default false
     */
    marketAppsLoading: false
  },
  /**
   * getters
   * @type {Object}
   * @readonly
   * @description Store module getters
   * @example
   * // Access to the module getters, where <name> is the getter name
   * Store.getters['Apps/<name>']
   */
  getters: {
    /**
     * getApp
     * @description Retrieve an app by unique name
     * @type {function}
     * @param {string} name - App unique name
     * @return {(App|undefined)}
     */
    getApp: (state) => (name) => {
      // Find app in state.apps
      return state.apps.find(app => app.name === name)
    },
    /**
     * getMarketApp
     * @description Retrieve a market app by unique name
     * @type {function}
     * @param {string} name - App unique name
     * @return {(App|undefined)}
     */
    getMarketApp: (state) => (name) => {
      // Find app in state.marketApps
      return state.marketApps.find(app => app.name === name)
    }
  },
  /**
   * actions
   * @type {Object}
   * @protected
   * @description Store module actions
   * @example
   * // Access to the module action, where <name> is the action name
   * Store.dispatch('Apps/<name>')
   */
  actions: {
    /**
     * loadApps
     * @description Load apps from server.
     * Request `[GET] /api/apps` (See [API Server documentation](/guide/system/api-server/)).
     * @return {(boolean|Error)} If success: true, else Error
     */
    loadApps: async function ({ commit }) {
      // Set loading
      commit('SET_APPS_LOADING', true)
      let appsResponse = null
      let apps = null
      let toReturn = true
      try {
        // Request server
        appsResponse = await ServerAPI.request('GET', '/apps')
        apps = appsResponse.data
        // Commit received data
        commit('SET_APPS', apps)
      } catch (error) {
        toReturn = error
      }
      // Loading to false
      commit('SET_APPS_LOADING', false)
      return toReturn
    },
    /**
     * loadMarketApps
     * @description Load market apps from MyStation Web API.
     * Request `[GET] http://mystation.fr/api/apps`.
     * @return {(Error|Object[])} Return collection of app or Error
     */
    loadMarketApps: async function ({ commit }) {
      // Set loading
      commit('SET_MARKET_APPS_LOADING', true)
      let marketAppsResponse = null
      let marketApps = null
      let toReturn = true
      try {
        // Request server
        marketAppsResponse = await APIManager.use('myStationWebAPI').request('GET', '/apps')
        marketApps = marketAppsResponse.data
        // Commit received data
        commit('SET_MARKET_APPS', marketApps)
      } catch (error) {
        toReturn = error
      }
      // Loading to false
      commit('SET_MARKET_APPS_LOADING', false)
      return toReturn
    }
  },
  /**
   * mutations
   * @type {Object}
   * @protected
   * @description Store module mutations
   * @example
   * // Dispatch a module mutation, where <mutation_name> is the mutation name
   * Store.commit('Apps/<mutation_name>', [payload])
   */
  mutations: {
    /**
     * SET_APPS
     * @description Mutate state.apps
     * @param {Object[]} apps - Data received from server
     * @return {void}
     */
    SET_APPS (state, apps) {
      state.apps = apps.map((app) => {
        return new App(app)
      })
    },
    /**
     * SET_APPS_LOADING
     * @description Mutate state.appsLoading
     * @param {boolean} value - True or false
     * @return {void}
     */
    SET_APPS_LOADING (state, value) {
      state.appsLoading = value
    },
    /**
     * SET_MARKET_APPS
     * @description Mutate state.marketApps
     * @param {Object[]} apps - Data received from server
     * @return {void}
     */
    SET_MARKET_APPS (state, apps) {
      state.marketApps = apps.map((app) => {
        return new MarketApp(app)
      })
    },
    /**
     * SET_MARKET_APPS_LOADING
     * @description Mutate state.marketAppsLoading
     * @param {boolean} value - True or false
     * @return {void}
     */
    SET_MARKET_APPS_LOADING (state, value) {
      state.marketAppsLoading = value
    }
  }
}

export default Apps

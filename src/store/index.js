/**
 * @vuepress
 * ---
 * title: Store
 * headline: "MyStation store"
 * sidebarDepth: 0 # To disable auto sidebar links
 * prev: false # Disable prev link
 * next: false # Disable prev link
 * ---
 */

/**
 * MyStation store
 * @module store
 * @description
 * MyStation store: Vuex Store (see: [https://vuex.vuejs.org/guide/](https://vuex.vuejs.org/guide/))
 * ```javascript
 * // Import
 * import Store from '@/store'
 * ```
 */

import Vue from 'vue'
import Vuex from 'vuex'
// Modules
import Users from './modules/Users'
import Apps from './modules/Apps'
// APIManager
import APIManager from '#mystation/services/api-manager/index'

Vue.use(Vuex)

// Define MyStation local API
const ServerAPI = APIManager.use('myStationAPI')
const MyStationWebAPI = APIManager.use('myStationWebAPI')

/**
 * Store
 * @alias module:store
 * @type {Vuex.Store}
 * @vuepress_syntax_block Store
 * @vuepress_syntax_desc Access to store instance
 * @vuepress_syntax_ctx {root}
 * this.$store
 * @vuepress_syntax_ctx {apps}
 * this.$store
 * __MYSTATION__.$store
 * @vuepress_syntax_ctx {any}
 * window.__MYSTATION_VUE_INSTANCE__.$store
 */
const Store = new Vuex.Store({
  /**
   * modules
   * @alias module:store.modules
   * @type {Object}
   * @protected
   * @description Vuex Store option: store modules
   */
  modules: {
    Users,
    Apps
  },
  /**
   * state
   * @alias module:store.state
   * @type {Object}
   * @readonly
   * @description Vuex Store option. See syntax for accessor.
   * @vuepress_syntax_block Store.state
   * @vuepress_syntax_desc Access to property
   * @vuepress_syntax_ctx {root}
   * this.$store.state
   * @vuepress_syntax_ctx {apps}
   * this.$store.state
   * __MYSTATION__.$store.state
   * @vuepress_syntax_ctx {any}
   * window.__MYSTATION_VUE_INSTANCE__.$store.state
   * @example
   * // Access to the root state
   * Store.state
   * // Access to a module state, where <module_name> is the module name
   * Store.state.<module_name>
   */
  state: {
    /**
     * lastVersion
     * @description MyStation last version (got from MyStation Web API)
     * @type {string}
     */
    lastVersion: null, // Default to null
    /**
     * system
     * @description MyStation system data
     * @type {Object}
     */
    system: {
      /**
       * mustAdminPasswordBeChanged
       * @description Determine if admin password must be changed
       * @type {boolean}
       * @default false
       */
      mustAdminPasswordBeChanged: false,
      /**
       * currentApp
       * @description The current app displayed in mainframe
       * @type {App}
       * @default null
       */
      currentApp: null,
      /**
       * mainframeContent
       * @description The name of the component displayed in mainframe
       * @type {string}
       * @default null
       */
      mainframeContent: null,
      /**
       * dataLoading
       * @description System data loading indicator
       * @type {boolean}
       * @default false
       */
      dataLoading: false,
      /**
       * msVersion
       * @description MyStation version
       * @type {string}
       * @default null
       */
      msVersion: null,
      /**
       * serverVersion
       * @description MyStation server api version
       * @type {string}
       * @default null
       */
      serverVersion: null,
      /**
       * clientIpAddr
       * @description MyStation local ip address
       * @type {string}
       * @default null
       */
      clientIpAddr: null,
      /**
       * database
       * @description MyStation database informations
       * @property {?Number} [size = null] - Database size
       * @type {Object}
       */
      database: {
        size: null,
        apps: null
      }
    }
  },
  /**
   * getters
   * @alias module:store.getters
   * @type {Object}
   * @readonly
   * @description Vuex Store option. See syntax for accessor.
   * @vuepress_syntax_block Store.getters
   * @vuepress_syntax_desc Access to property, where `<name>` is the getter name
   * @vuepress_syntax_ctx {root}
   * this.$store.getters[<name>]
   * @vuepress_syntax_ctx {apps}
   * this.$store.getters[<name>]
   * __MYSTATION__.$store.getters[<name>]
   * @vuepress_syntax_ctx {any}
   * window.__MYSTATION_VUE_INSTANCE__.$store.getters[<name>]
   * @example
   * // Access to the root getter
   * Store.getters[<name>]
   * // Access to a module getter, where <getter_name> is the getter name
   * // and <module_name> is the module name
   * Store.getters['<module_name>/<getter_name>']
   */
  getters: {
    // Nothing for the moment
  },
  /**
   * actions
   * @alias module:store.actions
   * @type {Object}
   * @protected
   * @description Vuex Store option. See syntax for accessor.
   * @vuepress_syntax_block Store.dispatch(<name>, [<payload>])
   * @vuepress_syntax_desc Access to property, where `<name>` is the action name
   * @vuepress_syntax_ctx {root}
   * this.$store.dispatch(<name>, [<payload>])
   * @vuepress_syntax_ctx {apps}
   * this.$store.dispatch(<name>, [<payload>])
   * __MYSTATION__.$store.dispatch(<name>, [<payload>])
   * @vuepress_syntax_ctx {any}
   * window.__MYSTATION_VUE_INSTANCE__.$store.dispatch(<name>, [<payload>])
   * @example
   * // Dispatch a root action, where <name> is the action name
   * Store.dispatch(<name>, [<payload>])
   * // Dispatch a module action, where <module_name> is the module name
   * // and <action_name> is the action name
   * Store.dispatch('<module_name>/<action_name>', [<payload>]) // equivalent
   */
  actions: {
    /**
     * switchMainframe
     * @description Switch mainframe component.
     * If switch to an app, set currentApp with App object from state.apps, else set to null.
     * In both cases, set mainframeComponent to `<component>` value.
     * @param {string} component - The name of the component to include in the main frame.
     * Must be 'Dashboard', 'Settings', 'AppMarket' or an app name.
     * @return {void}
     * @example
     * // Display dashboard in UI
     * Store.dispatch('switchMainframe', 'Dashboard')
     */
    switchMainframe: function ({ commit, getters }, component) {
      commit('SET_MAINFRAME_COMPONENT', null) // Reset to null before
      commit('SET_CURRENT_APP', null) // Reset current app to null before
      if (component !== 'Dashboard' && component !== 'Settings' && component !== 'AppMarket') {
        // App name
        const app = getters['Apps/getApp'](component)
        // App component name
        component = app.name + '-app-component'
        // Current app
        commit('SET_CURRENT_APP', app)
      } else {
        commit('SET_CURRENT_APP', null)
      }
      commit('SET_MAINFRAME_COMPONENT', component)
    },
    /**
     * loadSystemData
     * @description Get system data from server.
     * Request `[GET] /api/version`
     * @return {(boolean|Error)}
     * @example
     * // load system data
     * Store.dispatch('loadSystemData')
     */
    loadSystemData: async function ({ commit }) {
      commit('SET_SYSTEM_DATA_LOADING', true)
      let serverResponse
      // Request for auth
      try {
        // First, version
        serverResponse = await ServerAPI.request('GET', '/system/version')
        // Set version
        commit('SET_SYSTEM_MSVERSION', serverResponse.data.system)
        commit('SET_SYSTEM_SERVER_VERSION', serverResponse.data.server)
        // Then, network infos
        serverResponse = await ServerAPI.request('GET', '/system/network')
        commit('SET_SYSTEM_CLIENT_IP_ADDR', serverResponse.data.clientIpAddr)
        commit('SET_SYSTEM_SANGO', serverResponse.data.sango)
        // Then, database informations
        serverResponse = await ServerAPI.request('GET', '/system/database')
        // Set database informations
        commit('SET_SYSTEM_DATABASE_SIZE', Number(serverResponse.data.size))
        commit('SET_SYSTEM_DATABASE_APPS', serverResponse.data.apps)
        // Set dataLoading to false
        commit('SET_SYSTEM_DATA_LOADING', false)
        return true
      } catch (error) {
        // Set dataLoading to false
        commit('SET_SYSTEM_DATA_LOADING', false)
        return error
      }
    },
    /**
     * changeAdminPassword
     * @description Change admin password.
     * Request `[POST] /api/changeAdminPassword`
     * @param {Object} payload - current and new password
     * @return {(boolean|Error)}
     * @example
     * // load system data
     * Store.dispatch('changeAdminPassword', { currentPassword: 'psswd', newPassword: 'psswd'})
     */
    changeAdminPassword: async function ({ commit }, payload) {
      let serverResponse
      // Request for auth
      try {
        serverResponse = await ServerAPI.request('POST', '/changeAdminPassword', {
          data: payload
        })
        commit('SET_MUST_ADMIN_PASSWORD_BE_CHANGED', false)
        return serverResponse
      } catch (error) {
        return error
      }
    },
    /**
     * getLastVersion
     * @description Get last version from MyStaion Web API server.
     * Request `[GET] http://mystation.fr/api/version/current`
     * @return {(boolean|Error)}
     * @example
     * // Get version
     * Store.dispatch('getLastVersion')
     */
    getLastVersion: async function ({ commit }) {
      // Request for update
      try {
        const response = await MyStationWebAPI.request('GET', '/version/current')
        commit('SET_LAST_VERSION', response.data.version)
      } catch (error) {
        return error
      }
      return true
    },
    /**
     * updateSystem
     * @description Update system.
     * Request `[POST] /api/system/update`
     * @param {Object} payload - 'force' option
     * @return {(boolean|Error)}
     * @example
     * // Update system
     * Store.dispatch('updateSystem', { force: true }) // force: false by default
     */
    updateSystem: async function ({ commit }, payload) {
      // Request for update
      try {
        await ServerAPI.request('POST', '/system/update', {
          data: payload
        })
      } catch (error) {
        return error
      }
      // Reload page
      window.location.reload()
    },
    /**
     * checkHttp
     * @description Check http response.
     * Request `[GET] <url>` (or API baseURL if payload.apiName is defined)
     * @param {Object} payload
     * @param {string} payload.url - Required if apiName is not defined
     * @param {string} [payload.apiName] - Already set API name. Priority on url.
     * @return {(boolean|Error)}
     * @example
     * // Check API
     * Store.dispatch('checkHttp', { apiName: 'myStationWebApi', url: '/' })
     * // Check url directly
     * Store.dispatch('checkHttp', { url: 'https://8.8.8.8/' })
     */
    checkHttp: async function ({ commit }, payload) {
      let response
      let url
      // Request for update
      try {
        if (payload.apiName) {
          url = APIManager.use(payload.apiName).driver.defaults.baseURL + (payload.url ? payload.url : '')
        } else {
          url = payload.url
        }
        response = await fetch(url, {
          mode: 'no-cors'
        })
      } catch (error) {
        return error
      }
      return response
    }
  },
  /**
   * mutations
   * @alias module:store.mutations
   * @type {Object}
   * @protected
   * @description Vuex Store option. See syntax for accessor.
   * @vuepress_syntax_block Store.commit(<name>, [<payload>])
   * @vuepress_syntax_desc Access to property, where `<name>` is the mutation name
   * @vuepress_syntax_ctx {root}
   * this.$store.commit(<name>, [payload])
   * @vuepress_syntax_ctx {apps}
   * this.$store.commit(<name>, [payload])
   * __MYSTATION__.$store.commit(<name>, [payload])
   * @vuepress_syntax_ctx {any}
   * window.__MYSTATION_VUE_INSTANCE__.$store.commit(<name>, [payload])
   * @example
   * // Commit a root mutation, where <name> is the mutation name
   * Store.commit(<name>, [payload])
   * // Dispatch a module mutation, where <module_name> is the module name
   * // and <mutation_name> is the mutation name
   * Store.commit('<module_name>/<mutation_name>', [payload])
   */
  mutations: {
    /**
     * SET_LAST_VERSION mutation
     * @description Mutate Store.state.lastVersion
     * @param {string} value
     * @return {void}
     */
    SET_LAST_VERSION (state, value) {
      state.lastVersion = value
    },
    /**
     * SET_MUST_ADMIN_PASSWORD_BE_CHANGED mutation
     * @description Mutate Store.state.system.mustAdminPasswordBeChanged
     * @param {boolean} value
     * @return {void}
     */
    SET_MUST_ADMIN_PASSWORD_BE_CHANGED (state, value) {
      state.system.mustAdminPasswordBeChanged = value
    },
    /**
     * SET_CURRENT_APP mutation
     * @description Mutate Store.state.system.currentApp
     * @param {?App} app - The current app to set
     * @return {void}
     */
    SET_CURRENT_APP (state, app) {
      state.system.currentApp = app
    },
    /**
     * SET_MAINFRAME_COMPONENT mutation
     * @description Mutate Store.state.system.mainframeContent
     * @param {?string} component - Component name
     * @return {void}
     */
    SET_MAINFRAME_COMPONENT (state, component) {
      state.system.mainframeContent = component
    },
    /**
     * SET_SYSTEM_DATA_LOADING mutation
     * @description Mutate Store.state.system.dataLoading
     * @param {boolean} value
     * @return {void}
     */
    SET_SYSTEM_DATA_LOADING (state, value) {
      state.system.dataLoading = value
    },
    /**
     * SET_SYSTEM_MSVERSION mutation
     * @description Mutate Store.state.system.msVersion
     * @param {string} value
     * @return {void}
     */
    SET_SYSTEM_MSVERSION (state, value) {
      state.system.msVersion = value
    },
    /**
     * SET_SYSTEM_SERVER_VERSION mutation
     * @description Mutate Store.state.system.serverVersion
     * @param {string} value
     * @return {void}
     */
    SET_SYSTEM_SERVER_VERSION (state, value) {
      state.system.serverVersion = value
    },
    /**
     * SET_SYSTEM_CLIENT_IP_ADDR mutation
     * @description Mutate Store.state.system.clientIpAddr
     * @param {string} value
     * @return {void}
     */
    SET_SYSTEM_CLIENT_IP_ADDR (state, value) {
      state.system.clientIpAddr = value
    },
    /**
     * SET_SYSTEM_SANGO mutation
     * @description Mutate Store.state.system.sango
     * @param {Object} value
     * @return {void}
     */
    SET_SYSTEM_SANGO (state, value) {
      state.system.sango = value
    },
    /**
     * SET_SYSTEM_DATABASE_SIZE mutation
     * @description Mutate Store.state.system.database.size
     * @param {Number} value
     * @return {void}
     */
    SET_SYSTEM_DATABASE_SIZE (state, value) {
      state.system.database.size = value
    },
    /**
     * SET_SYSTEM_DATABASE_APPS mutation
     * @description Mutate Store.state.system.database.apps
     * @param {Object} value
     * @return {void}
     */
    SET_SYSTEM_DATABASE_APPS (state, value) {
      state.system.database.apps = value
    }
  }
})

export default Store

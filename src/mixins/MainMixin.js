/*   Main mixin   */

const mixin = {
  computed: {
    GLOBALS: function () {
      return this.$root.ROOT_GLOBALS
    }
  }
}

export default mixin

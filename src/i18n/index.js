/**
 * @vuepress
 * ---
 * title: I18n
 * headline: "MyStation Internationalization (I18n)"
 * sidebarDepth: 0 # To disable auto sidebar links
 * prev: false # Disable prev link
 * next: false # Disable prev link
 * ---
 */

/**
 * MyStation i18n
 * @module i18n
 * @description
 * MyStation internationalization: VueI18n (see: [https://kazupon.github.io/vue-i18n/](https://kazupon.github.io/vue-i18n/))
 * ```javascript
 * // Import
 * import I18n from '@/i18n'
 * ```
 */

import Vue from 'vue'
import VueI18n from 'vue-i18n'
// Configuration
import CONFIG from '@/config/config.js'
// Import main App internationalization
import en from './en'
import fr from './fr'
// Import ErrorRefs for ErrorManager plugin
import errors_en from './errors/en' // eslint-disable-line camelcase
import errors_fr from './errors/fr' // eslint-disable-line camelcase

Vue.use(VueI18n)

// Merge errors
en.errors = errors_en // eslint-disable-line camelcase
fr.errors = errors_fr // eslint-disable-line camelcase

// Add 'apps' empty object
en.apps = {}
fr.apps = {}

const messages = {
  en,
  fr
}

/**
 * I18n
 * @alias module:i18n
 * @type {VueI18n}
 * @vuepress_syntax_block I18n
 * @vuepress_syntax_desc Access to i18n instance
 * @vuepress_syntax_ctx {root}
 * this.$i18n
 * @vuepress_syntax_ctx {apps}
 * this.$i18n
 * __MYSTATION__.$i18n
 * @vuepress_syntax_ctx {any}
 * window.__MYSTATION_VUE_INSTANCE__.$i18n
 * @example
 * // Change locale
 * this.$i18n.locale = 'en'
 * // Translate
 * this.$t('path.to.message') // in a component
 * // Learn more about vue-i18n at https://kazupon.github.io/vue-i18n/
 */
const I18n = new VueI18n({
  /**
   * locale
   * @alias module:i18n.locale
   * @type {string}
   * @description VueI18n option: locale. See: [Configuration](/guide/ui/internal-flow/#configuration) for default value
   * @default fr
   */
  locale: CONFIG.I18N.LOCALE,
  /**
   * fallbackLocale
   * @alias module:i18n.fallbackLocale
   * @type {string}
   * @description VueI18n option: fallbackLocale. See: [Configuration](/guide/ui/internal-flow/#configuration) for default value
   * @default en
   */
  fallbackLocale: CONFIG.I18N.FALLBACK_LOCALE,
  /**
   * messages
   * @alias module:i18n.messages
   * @type {Object}
   * @description VueI18n option: messages.
   * The translations for each app are registered asynchronously
   * in `<locale>.apps.<app_name>` property.
   * See [AppManager service documentation](/guide/ui/api/mystation/services/AppManager).
   * @property {Object} en - MyStation English translations
   * @property {Object} fr - MyStation French translations
   */
  messages
})

export default I18n

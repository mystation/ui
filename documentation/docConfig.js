module.exports = {
  pathPrefix: '/guide/ui/api/',
  treeMap: {
    components: {
      title: 'Components'
    },
    i18n: {
      title: 'I18n'
    },
    mystation: {
      title: 'MyStation'
    },
    store: {
      title: 'Store'
    }
  }
}

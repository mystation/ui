exports.defineTags = function (dictionary) {
  dictionary.defineTag('vuepress_syntax_block', {
    mustHaveValue: true,
    mustNotHaveDescription: false,
    canHaveType: false,
    canHaveName: true,
    onTagged: function (doclet, tag) {
      doclet.syntax = {
        title: tag.value.description ? tag.value.name + ' ' + tag.value.description : tag.value.name,
        description: 'No description',
        context: {}
      }
    }
  })
  dictionary.defineTag('vuepress_syntax_desc', {
    canHaveType: false,
    canHaveName: true,
    onTagged: function (doclet, tag) {
      if (doclet.syntax) {
        // Get syntax description:
        doclet.syntax.description = tag.value.description ? tag.value.name + ' ' + tag.value.description : tag.value.name
      }
    }
  })
  dictionary.defineTag('vuepress_syntax_ctx', {
    canHaveType: true,
    onTagged: function (doclet, tag) {
      if (doclet.syntax) {
        // Get context identifier as a type:
        // ex:
        // * @vuepress_syntax_ctx {root}
        // * ...description
        // * ...
        const ctxIdentifier = tag.value.type.names[0]
        // Get description
        const ctxDescription = tag.value.description
        // Replace '\r' by '\n'
        doclet.syntax.context[ctxIdentifier] = ctxDescription.replace('\r', '\n')
      }
    }
  })
}

exports.handlers = {
  newDoclet: function (e) {
    // Do something when we see a new doclet
    // console.log('Oh! we found a new doclet! :)')
  }
}

'use strict';

const docConfig = require('../../docConfig.js')
const treeMap = docConfig.treeMap

// create vuepress sidebar
module.exports = ({ fileTree, codeFolder, title }) => {
  // let rootFiles = [['', '::vuepress-jsdoc-title::']];
  // let rootFiles = [];
  // rootFiles = rootFiles.concat(fileTree.filter(file => !file.children).map(file => file.name));

  let rootFolder = fileTree.filter(file => file.children && file.children.length > 0);

  function buildItem(item) {
    let newItem = {}
    // title
    if (treeMap[item.name]) {
      newItem.title = treeMap[item.name].title
    } else {
      newItem.title =  item.name
    }
    // Children ?
    let itemChildren = [];
    if (item.children && item.children.length > 0) {
      // Loop on children
      item.children.forEach(child => {
        // Does the folder have README children?
        if (child.name === 'README') {
          // Item path
          newItem.path = docConfig.pathPrefix + child.fullPath.replace('README', '')
        } else {
          itemChildren.push(buildItem(child))
        }
      });
    } else if (item.fullPath) {
      // Item path
      newItem.path = docConfig.pathPrefix + item.fullPath
    }

    if (itemChildren.length > 0) {
      newItem.children = itemChildren
    }

    return newItem;
  }

  const tree = rootFolder.map(folder => {
    return buildItem(folder)
    // Does the folder have README children?
    let readme = folder.children.find(child => child.name === 'README')
    if (readme) {
      return {
        title: treeMap[folder.name].title,
        path: docConfig.pathPrefix + folder.name,
        children: buildChildren(folder.children, folder.name, 0)
      }
    } else {
      return {
        title: treeMap[folder.name].title,
        children: buildChildren(folder.children, folder.name, 0)
      }
    }
  });

  console.log(tree)

  // return {
  //   [`/${codeFolder}/`]: [
  //     {
  //       title,
  //       collapsable: false,
  //       children: rootFiles
  //     }
  //   ].concat(tree)
  // };
  return {
    [`/${codeFolder}/`]: tree
  };
};

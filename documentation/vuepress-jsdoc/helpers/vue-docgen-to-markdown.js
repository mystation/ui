/* eslint-disable */

const vueDocs = require('vue-docgen-api');

const customTags = { 
  'vuepress_syntax_block': function (items, parent) {
    // Take first item
    const tag = items[0]
    const syntaxTitle = tag.description
    parent.syntax = {
      title: syntaxTitle,
      description: 'No description',
      context: {
        // Will be filled with something like:
        // <context_identifier>: [lines...],
        // <context_identifier>: [lines...],
        // ...
      }
    }
    // Delete
    delete parent.tags['vuepress_syntax_block']
    return parent
  },
  'vuepress_syntax_desc': function (items, parent) {
    // Check if parent has 'vuepress_syntax_block'
    if (parent.syntax) {
      // Take first item
      const tag = items[0]
      parent.syntax.description = tag.description
    }
    // Delete
    delete parent.tags['vuepress_syntax_desc']
    return parent
  },
  'vuepress_syntax_ctx': function (items, parent) {
    // Check if parent has 'vuepress_syntax_block'
    if (parent.syntax) {
      items.forEach((item) => {
        const ctxIdentifier = item.description.match(/^{(.*)}/)[1]
        const ctxDefs = item.description.replace(/^{(.*)}/, '').split('\n')
        ctxDefs.shift()
        parent.syntax.context[ctxIdentifier] = ctxDefs
      })
    }
    // Delete
    delete parent.tags['vuepress_syntax_ctx']
    return parent
  },
  'vuepress_global_component': function (items, parent) {
    parent.globalComponent = true
    // Delete
    delete parent.tags['vuepress_global_component']
    return parent
  }
}

function processCustomTags (obj) {
  let tmp = obj
  if (obj.tags) {
    for (let key in obj.tags) {
      // If custom tag
      if (customTags[key]) {
        console.log('custom tag found : ', key)
        tmp = customTags[key](obj.tags[key], tmp)
      }
    }
  }
  return tmp
}

function paramsString(params) {
  if (params) {
    return params.map(param => `${param.name}: \`${param.type.name}\``).join(',');
  } else {
    return ''
  }
}

function generateTags({ tags }) {
  // ! MODIFIED
  return ''
  // ignore ...
  if (tags) {
    let tagsContent = '::: tip Tags\n';

    tagsContent += Object.keys(tags).map(key => {
      return tags[key].map(tag => {
        return `**${tag.title}**: ${tag.description}<br />`
      }).join('')
    }).join('')

    return tagsContent + '\n:::\n';
  }

  return '';
}

function fileContent() {
  let contentArray = [];
  let line = 0;

  return {
    get content() {
      return contentArray.join('\n');
    },
    addline(content) {
      contentArray[line] = content;
      line++;
    }
  };
}

module.exports = async path => {
  const file = fileContent();

  try {
    let data = await vueDocs.parse(path);

    data = processCustomTags(data)

    // file.addline(`# ${data.displayName}\n${data.description}\n`);

    file.addline(`# ${data.displayName}\n`);

    // Global component indicator
    if (data.globalComponent) {
      file.addline(`> Global registered component: \`<${data.displayName}></${data.displayName}>\`\n`);
    }

    file.addline(`${data.description}\n`);

    // Tags
    file.addline(generateTags(data));

    // Separator
    file.addline(`----\n`);

    // file.addline('## Table of contents\n[[toc]]\n');
    if (data.props || data.methods || data.slots || data.events) {
      file.addline('<h2>Summary</h2>\n');
      file.addline('[[toc]]\n');
    }

    // Props
    if (data.props) {
      const props = data.props;
      let propsContent = '## Props\n\n';

      props.forEach(prop => {
        propsContent += `### ${prop.name} (\`${prop.type.name}\`)\n`;

        // Tags
        prop = processCustomTags(prop)
        propsContent += generateTags(prop);

        propsContent += prop.description;

        propsContent += '\n\n|type|default|\n|:-|:-|\n';
        propsContent += `|\`${prop.type.name}\`|${prop.defaultValue ? prop.defaultValue.value : '-'}|\n\n`;

        // Syntax
        if (prop.syntax) {
          propsContent += '::: syntax ' + prop.syntax.title + '\n'
          propsContent += prop.syntax.description + '\n'
          for (let ctx in prop.syntax.context) {
            propsContent += '- <span class="color-green">{ _' + ctx + ' context_ }</span>\n'
            propsContent += '  ```javascript\n'
            for (let line in prop.syntax.context[ctx]) {
              propsContent += '  ' + prop.syntax.context[ctx][line] + '\n'
            }
            propsContent += '  ```\n'
          }
          propsContent += ':::\n\n'
        }
      });

      file.addline(propsContent + '\n');
    }

    // Methods
    if (data.methods) {
      const methods = data.methods;
      file.addline('## Methods\n');

      methods.forEach(method => {
        file.addline(
          `### ${method.name} (${paramsString(method.params)}) ⇒ \`${method.returns.type.name}\`\n ${
            method.description
          }\n`
        );

        // Tags
        file.addline(generateTags(method));

        // params
        if (method.params) {
          file.addline(
            `<span class="color-beige">Params:</span>\n| name | type | description\n|:-|:-|:-|\n` +
              method.params.map(param => `|${param.name}|\`${param.type.name}\`|${param.description}`).join('\n')
          );
        }

        // returns
        const returnDesc = method.returns.description || ''
        file.addline(`\n<span class="color-beige">Returns:</span> (${method.returns.type.name})\n ${returnDesc}`);
      });
    }

    // Slots
    if (data.slots) {
      const slots = data.slots;
      file.addline('## Slots\n');

      file.addline(`${slots.map(slot => `### ${slot.name}\n${slot.description}`).join('\n')}\n\n`);
    }

    // Events
    if (data.events) {
      const events = data.events;
      let eventsContent = '## Events\n\n';

      events.forEach(event => {
        eventsContent += `### ${event.name} (${event.type.names.join(',')})\n\n${event.description}\n`;

        // properties
        if (event.properties) {
          eventsContent += `#### Properties\n| name | type | description\n|:-|:-|:-|\n${event.properties
            .map(property => `|${property.name}|\`${property.type.names.join(',')}\`|${property.description}`)
            .join('\n')}`;
        }
      });

      file.addline(eventsContent + '\n\n');
    }

    return Promise.resolve(file.content);
  } catch (err) {
    return Promise.reject(err);
  }
};

/**
 * jsdoc custom tag parser for vuepress theme
 * Used by vuepress-jsdoc
 */
const syntaxCommentParser = data => {
  console.log('data : ', data)
  const commentBlocks = findCommentBlocks(data)
  // For each found comment, find only syntax-tagged blocks
  const syntaxTaggedBlocks = commentBlocks.filter((block) => {
    return hasSyntaxTag(block)
  })
  // For each found syntax-tagged block
  syntaxTaggedBlocks.forEach(block => {
    let lines
    let syntaxTitle
    let syntaxDesc = 'No description'
    let syntaxCtx = []
    // Tag line index
    let tagLineIndex = hasSyntaxTag(block)
    // get lines
    lines = block.split('\n')
    // If syntax tag found
    if (tagLineIndex) {
      // First, title
      // Syntax block title
      syntaxTitle = lines[tagLineIndex].match(/@vuepress_syntax_block (.*)/)[1] || ''
      // Prepare string for replace
      let replace = [
        '* ::: syntax ' + syntaxTitle, // first line
      ]
      // For each line
      lines.forEach((line) => {
        // Is there description ?
        // find '@vuepress_syntax_desc'
        let descTag = line.match(/@vuepress_syntax_desc (.*)/)
        if (descTag) {
          syntaxDesc = descTag[1]
        }
        // Are there contexts ?
        // find '@vuepress_syntax_ctx'
        let ctx = line.match(/@vuepress_syntax_ctx {(.*)} (.*)/)
        if (ctx) {
          syntaxCtx.push('* - <span class="color-green">{ _' + ctx[1] + ' context_ }</span>')
          syntaxCtx.push('* ```javascript')
          syntaxCtx.push('* ' + ctx[2])
          syntaxCtx.push('* ```')
        }
      })
      // Add description to replace
      replace.push('* > ' + syntaxDesc)
      // Add ctx to replace
      replace.push(...syntaxCtx)
      // Close replace
      replace.push('* :::') // last line
      console.log('replace : ', replace)
    }
  })
}

/**
 * Extract only comment blocks from file content
 * @param {string} str File content to parse
 * @return {Array<String>}
 */
function findCommentBlocks (str) {
  // Find each comment block
  let allCommentBlocks = str.match(/\/\*[\s\S]*?\*\/|([^:]|^)\/\/.*$/g)
  return allCommentBlocks
}

/**
 * Search '@vuepress_syntax_block' tag
 * @param {String} block - The comment block
 * @return {Boolean|Number} False if no tag found, else tag line index
 */
function hasSyntaxTag (block) {
  const lines = block.split('\n')
  let tagLineIndex = false
  lines.forEach((line, index) => {
    // find '@vuepress_syntax_block'
    if (line.indexOf('@vuepress_syntax_block') >= 0) {
      tagLineIndex = index
    }
  })
  return tagLineIndex
}

module.exports = syntaxCommentParser

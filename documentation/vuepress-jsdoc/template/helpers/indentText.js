/**
 * Indent a string
 * -> add <spaceNumber> of white space before each line of a string
 * separator: '\n'
 */
exports.indentText = function (text, spaceNumber = 2) {
  let tmp = []
  text.split('\n').forEach((line) => {
    tmp.push((' ').repeat(spaceNumber) + line)
  })
  return tmp.join('\n')
}

/* eslint-disable */

const path = require('path')
const fs = require('fs')
const colors = require('colors')
const prompts = require('prompts')
const gitStatus = require("git-status")

const CONFIG = require('../globals.config.json')
const packageJson = require('../package.json')

const CURRENT_VERSION = CONFIG.VERSION.CURRENT

function incrementVersion (version, type = 'patch') {
  const schema = ['major', 'minor', 'patch']
  return version.split('.').map((n, index) => {
    if (type === schema[index]) {
      return Number(n) + 1
    } else {
      return n
    }
  }).join('.')
}

// --

module.exports = async () => {
  // Start
  console.log('- Script: new-version - started ▶'.cyan)
  console.log('----------------------------------\n'.cyan)

  console.log('MyStation UI current version: ' + CURRENT_VERSION + '\n')

  // Check git status
  console.log('> Check git status...\n')
  const changes = await (new Promise((resolve, reject) => { 
    gitStatus((err, data) => {
      // => [ { x: ' ', y: 'M', to: 'example/index.js', from: null } ]
      if (err) {
        reject()
      } else {
        resolve(data)
      }
    })
  }))

  // If git repo clean
  if (changes.length === 0) {

    console.log('✔ Git Status OK\n'.green)

    // Version prompts
    const userPrompts = await prompts([
      {
        type: 'select',
        name: 'type',
        message: 'New version type:',
        choices: [
          { title: 'patch', value: 'patch' },
          { title: 'minor', value: 'minor' },
          { title: 'major', value: 'major' }
        ]
      },
      {
        type: 'text',
        name: 'version',
        message: 'New version (current: ' + CURRENT_VERSION + '):',
        initial: (prev, values) => incrementVersion(CURRENT_VERSION, values.type),
        validate: value => /^\d{1,3}.\d{1,3}.\d{1,3}$/.test(value)
      }
    ])
    console.log('\n')

    const newVersion = userPrompts.version

    if (newVersion) {
      console.log('New version: ' + newVersion + '\n')

      CONFIG.VERSION.CURRENT = newVersion
      packageJson.version = newVersion

      const filePaths = {
        config: path.join(__dirname, '../globals.config.json'),
        packageJson: path.join(__dirname, '../package.json'),
        versionFile: path.join(__dirname, '../public/VERSION')
      }
    
      console.log('> Update ' + filePaths.config + '\n')
      // Update globals.config.json
      fs.writeFileSync(filePaths.config, JSON.stringify(CONFIG, null, 2), 'utf8')
    
      console.log('> Update ' + filePaths.packageJson + '\n')
      // Update package.json
      fs.writeFileSync(filePaths.packageJson, JSON.stringify(packageJson, null, 4), 'utf8')

      console.log('> Update ' + filePaths.versionFile + '\n')
      // Update public/VERSION
      fs.writeFileSync(filePaths.versionFile, newVersion, 'utf8')

    }

  } else {
    console.log('❌ Git status KO: '.red, changes)
    throw new Error('Git status is not OK. Please commit your changes'.red)
  } // end if git status clean

  // End
  console.log('----\n'.cyan)
}

module.exports = {
  "presets": [
    ["@babel/preset-env", {
      "modules": false,
      "targets": {
        "browsers": ["> 1%", "last 2 versions", "not ie <= 8"]
      },
      "useBuiltIns": "usage"
    }]
  ],
  "plugins": [
    "transform-vue-jsx", // Required for Vue projects
    "@babel/plugin-transform-runtime",
    "@babel/plugin-proposal-class-properties", // To enable ES6 javascript classes syntax
    // ["@babel/plugin-proposal-decorators", { "legacy": true }],
    // "@babel/plugin-proposal-function-sent",
    // "@babel/plugin-proposal-export-namespace-from",
    // "@babel/plugin-proposal-numeric-separator",
    // "@babel/plugin-proposal-throw-expressions"
  ],
  "env": {
    // "test": {
    //   "presets": [
    //     ["@babel/preset-env", {
    //       "modules": "commonjs",
    //       "targets": {
    //         "browsers": ["> 1%", "last 2 versions", "not ie <= 8"]
    //       }
    //     }]
    //   ],
    //   "plugins": [
    //     "transform-vue-jsx", 
    //     "@babel/plugin-transform-runtime",
    //     "@babel/plugin-proposal-class-properties",
    //     ["@babel/plugin-proposal-decorators", { "legacy": true }],
    //     "@babel/plugin-proposal-function-sent",
    //     "@babel/plugin-proposal-export-namespace-from",
    //     "@babel/plugin-proposal-numeric-separator",
    //     "@babel/plugin-proposal-throw-expressions"
    //   ],
    // },
    // "production": {
    //   "presets": ["minify"]
    // }
  }
}
